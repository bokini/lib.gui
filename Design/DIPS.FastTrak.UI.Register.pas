{ ------------------------------------------------------------- }
{ Purpose : DIPS FastTrak UI }
{ By      : Bojan Nikolic }
{ For     : DIPS AS }
{ ------------------------------------------------------------- }
{ Copyright (C) DIPS AS 2020. All Rights Reserved. }
{ ------------------------------------------------------------- }
unit DIPS.FastTrak.UI.Register;

interface

uses
  {Standard}
  Classes, Types, Forms, Graphics,
  {Standard.Design}
  DesignIntf, DesignEditors, VCLEditors, TypInfo, ImgList,
  {DIPS}
  DIPS.FastTrak.UI.Edit,
  DIPS.FastTrak.UI.Buttons,
  DIPS.FastTrak.UI.DatePicker;

procedure Register;

implementation

uses
  {Winapi}
  Winapi.Windows,
  {DIPS}
  DIPS.FastTrak.UI.Graphics;

procedure Register;
const
  PaletteCaption = 'FastTrak';
begin
  RegisterComponents( PaletteCaption, [TdcEdit, TdcDatePicker, TdcButtonEdit, TdcSpinEdit, TdcCheckBox, TdcComboBox] );
  RegisterComponents( PaletteCaption, [TdcExecuteButton, TdcDropDownButton] );
  RegisterComponents( PaletteCaption, [TdcTextValidator] );
end;

end.
