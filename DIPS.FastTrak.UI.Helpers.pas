unit DIPS.FastTrak.UI.Helpers;

interface

uses
  System.Types,
  Vcl.Graphics,
  Winapi.Windows,
  {DIPS}
  DIPS.FastTrak.UI.Consts, DIPS.FastTrak.UI.Types;

type
  { TCanvasHelper }

  TCanvasHelper = class helper for TCanvas
    procedure Erase( const AColor: TColor; const ARect: TRect );
    procedure MaskRect( const Value: TRect );
    procedure IsolateRect( const Value: TRect );
    procedure PaintDropDownButton( BtnRect: TRect; BtnState: TButtonState );
    procedure RenderText( TextRect: TRect; Text: string; TextFormat: TTextFormat );
  end;

implementation

{ TCanvasHelper }

procedure TCanvasHelper.Erase( const AColor: TColor; const ARect: TRect );
begin
  Brush.Color := AColor;
  FillRect( ARect );
end;

procedure TCanvasHelper.IsolateRect( const Value: TRect );
var
  CLPRGN: HRGN;
  P: TPoint;
begin
  with Value do
    CLPRGN := CreateRectRgn( Left, Top, Right, Bottom );
  try
    GetWindowOrgEx( Handle, P );
    OffsetRgn( CLPRGN, -P.X, -P.Y );
    SelectClipRgn( Handle, CLPRGN );
  finally
    DeleteObject( CLPRGN );
  end;
end;

procedure TCanvasHelper.MaskRect( const Value: TRect );
begin
  with Value do
    ExcludeClipRect( Handle, Left, Top, Left + ( Right - Left ), Top + ( Bottom - Top ) );
end;

procedure TCanvasHelper.PaintDropDownButton( BtnRect: TRect; BtnState: TButtonState );
var
  ButtonFace: TColor;
begin
  ButtonFace := clEditControl;
  if btHot in BtnState then
  begin
    ButtonFace := clBtnFaceDropDown;
    if btPushed in BtnState then
      ButtonFace := clBtnFaceNormalPressed;
  end;
  Brush.Color := ButtonFace;
  FillRect( BtnRect );
  Pen.Color := clGlyph;
  Brush.Color := Pen.Color;
  with BtnRect do Polygon( [Point( Left + 4, Top + 6 ),
    Point( Left + BtnRect.Width - 4, Top + 6 ), Point( Left + BtnRect.Width div 2, Top + BtnRect.Height - 6 )] );
end;

procedure TCanvasHelper.RenderText( TextRect: TRect; Text: string; TextFormat: TTextFormat );
var
  r: TRect;
  s: string;
begin
  s := Text;
  r := TextRect;
  Brush.Style := bsClear;
  Self.TextRect( r, s, TextFormat );
  Brush.Style := bsSolid;
end;

end.
