{ ------------------------------------------------------------- }
{ Purpose : DIPS FastTrak UI                                    }
{ By      : Bojan Nikolic                                       }
{ For     : DIPS AS                                             }
{ ------------------------------------------------------------- }
{ Copyright (C) BergSoft.net 2019. All Rights Reserved.         }
{ ------------------------------------------------------------- }
unit DIPS.FastTrak.UI.Types;

interface

type
  TButtonState = set of ( btChecked, btDisabled, btHot, btInvalid, btPushed, btReadOnly, btSelected );
  TDateRange = ( drAll, drPastOnly, drFutureOnly );
  TPopupAnchor = ( paLeft, paRight, paRightTop );
  TPopupState = set of ( psOpening, psClosing );
  TStartDayOfWeek = ( dwSystem, dwMonday, dwSunday );
  TValidationResult = ( vrUnchecked, vrValid, vrInvalid );
  TValidationTriggers = set of ( vtLeave, vtTyping, vtTextChanged );

  TEditOptions = set of (edPaste, edTyping);

  TNxWrapKind = ( wkNone, wkEllipsis, wkPathEllipsis, wkWordEllipsis, wkWordWrap );
  TNxNumericEditOptions = set of (ednDecimals, ednSigns);
  TNxDrawingOptions = set of ( doBackground, doContent, doCustom );
  TNxDropDownStyle = ( dsDropDown, dsDropDownList );

implementation

end.
