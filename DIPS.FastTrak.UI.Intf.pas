{ ------------------------------------------------------------- }
{ Purpose : DIPS FastTrak UI                                    }
{ By      : Bojan Nikolic                                       }
{ For     : DIPS AS                                             }
{ ------------------------------------------------------------- }
{ Copyright (C) DIPS AS 2020. All Rights Reserved.              }
{ ------------------------------------------------------------- }
unit DIPS.FastTrak.UI.Intf;

interface

uses
  System.Classes, System.SysUtils,
  Vcl.Controls, Vcl.Graphics,
  {Winapi}
  Winapi.Windows,
  {DIPS}
  DIPS.FastTrak.UI.Types;

type
  { IObjectConverter }

  IObjectConverter = interface
    ['{EF85FAF0-1FB3-4D5C-AE00-6C8170842EF2}']
    function ToObject: TObject;
  end;

  { IValueAccessors }

  IValueAccessors = interface
    ['{FB662DD3-D7F4-4009-B189-7E297519D524}']
    { Property Accessors }
    function GetAsBoolean: Boolean;
    function GetAsDateTime: TDateTime;
    function GetAsFloat: Double;
    function GetAsInteger: Integer;
    function GetAsString: string;
    procedure SetAsBoolean( const Value: Boolean );
    procedure SetAsDateTime( const Value: TDateTime );
    procedure SetAsFloat( const Value: Double );
    procedure SetAsInteger( const Value: Integer );
    procedure SetAsString( const Value: string );
    { Properties }
    property AsBoolean: Boolean read GetAsBoolean write SetAsBoolean;
    property AsDateTime: TDateTime read GetAsDateTime write SetAsDateTime;
    property AsFloat: Double read GetAsFloat write SetAsFloat;
    property AsInteger: Integer read GetAsInteger write SetAsInteger;
    property AsString: string read GetAsString write SetAsString;
  end;

  { ICheckPopupOwner }

  ICheckPopupOwner = interface
    ['{7ECA25DE-78FF-4B7D-8629-69D452968597}']
    procedure CheckBoxClick( const Index: Integer );
  end;

  { IInplaceEdit }

  IInplaceEdit = interface
    ['{A8A2B25D-8F7F-4F73-BE06-2783DDE00652}']
    function CanDispatchKey( const Key: Word ): Boolean;
    procedure DoExit;
    function GetHandle: HWND;
    function GetText: TCaption;
    function GetBoundsRect: TRect;
    procedure Assign( Source: TPersistent );
    procedure SetBounds( ALeft, ATop, AWidth, AHeight: Integer );
    procedure SetInplaceEdit( const Value: Boolean );
    procedure SetParent( AParent: TWinControl );
    procedure SetFocus;
    procedure SetText( const Value: TCaption );
    property InplaceEdit: Boolean write SetInplaceEdit;
    property Handle: HWND read GetHandle;
    property Parent: TWinControl write SetParent;
    property Text: TCaption read GetText write SetText;
  end;

  { ISelectNotify }

  ISelectNotify = interface
    ['{78BB90A1-FC5E-4ED1-B8CE-16042E49B337}']
    { Property Accessors }
    function GetOnSelect: TNotifyEvent;
    procedure SetOnSelect( const Value: TNotifyEvent );
    { Properties }
    property OnSelect: TNotifyEvent read GetOnSelect write SetOnSelect;
  end;

  { IDropDown }

  IDropDown = interface
    ['{5840F233-921B-4DDA-9EEB-830D910459E3}']
    { Property Accessors }
    function GetAutoClose: Boolean;
    function GetDroppedDown: Boolean;
    procedure SetDroppedDown( const Value: Boolean );
    { Methods }
    procedure DoCloseUp;
    procedure DoSelect;
    { Properties }
    property AutoClose: Boolean read GetAutoClose;
    property DroppedDown: Boolean read GetDroppedDown write SetDroppedDown;
  end;

  { IDropDownNotify }

  IDropDownNotify = interface
    ['{4BE4B134-C812-46EC-966F-B69A440C9C0A}']
    { Property Accessors }
    procedure SetOnDropDown( const Value: TNotifyEvent );
    { Properties }
    property OnDropDown: TNotifyEvent write SetOnDropDown;
  end;

  { IStyledDropDown }

  IStyledDropDown = interface
    ['{E5421D49-ADE6-482B-81A0-F739EDDD3FE2}']
    procedure SetStyle( const Value: TNxDropDownStyle );
  end;

  { IStringsControl }

  IStringsControl = interface
    ['{FBB198D8-A097-4AD5-9BDB-CA725C77226B}']
    function GetAutoComplete: Boolean;
    function GetDropDownCount: Integer;
    function GetItemHeight: Integer;
    function GetItemIndex: Integer;
    function GetItems: TStrings;
    function GetItemsAlignment: TAlignment;
    function GetListWidth: Integer;
    procedure SetAutoComplete( const Value: Boolean );
    procedure SetDropDownCount( const Value: Integer );
    procedure SetItemHeight( const Value: Integer );
    procedure SetItemIndex( const Value: Integer );
    procedure SetItems( const Value: TStrings );
    procedure SetItemsAlignment( const Value: TAlignment );
    { Methods }
    function MeasureItemHeight( const Index: Integer; const AWidth: Integer ): Integer;
    function PaintCell( const Index: Integer; CellRect: TRect; State: TOwnerDrawState ): Boolean;
    procedure TryItemIndex( const Index: Integer; Select: Boolean );
    { Properties }
    property AutoComplete: Boolean read GetAutoComplete write SetAutoComplete;
    property DropDownCount: Integer read GetDropDownCount write SetDropDownCount;
    property ItemHeight: Integer read GetItemHeight write SetItemHeight;
    property ItemIndex: Integer read GetItemIndex write SetItemIndex;
    property Items: TStrings read GetItems write SetItems;
    property ItemsAlignment: TAlignment read GetItemsAlignment write SetItemsAlignment;
    property ListWidth: Integer read GetListWidth;
  end;

  { IView }

  INxView = interface( IObjectConverter )
    ['{D6A8F78E-BBF5-4D8D-A58C-84B6C705F666}']
    { Property Getters }
    function GetCanvas: TCanvas;
    function GetClientRect: TRect;
    { Property Setters }
    procedure SetCanvas( const Value: TCanvas );
    procedure SetClientRect( const Value: TRect );
    { Methods }
    procedure Paint;
    { Properties }
    property Canvas: TCanvas read GetCanvas write SetCanvas;
    property ClientRect: TRect read GetClientRect write SetClientRect;
  end;

  { IViewOwner }

  IViewOwner = interface
    ['{1FBB192B-8F43-468D-B949-4AD97FB2BD4C}']
    function GetCanvas: TCanvas;
    function GetHandle: THandle;
    { Methods }
    procedure InvalidateRect( const Source: TRect );
    { Properties }
    property Canvas: TCanvas read GetCanvas;
    property Handle: THandle read GetHandle;
  end;

  { IObjectView }

  IObjectView = interface( INxView )
    ['{A9D2227E-1140-4F32-8783-F5A333A094BD}']
    { Property Accessors }
    function GetAsString: WideString;
    function GetControl: TWinControl;
    function GetFont: TFont;
    procedure SetAsString( const Value: WideString );
    procedure SetControl( const Value: TWinControl );
    procedure SetFont( const Value: TFont );
    { Methods }
    procedure Assign( Source: TObject );
    function CanDispatchKey( var Key: Word ): Boolean;
    procedure KeyDown( var Key: Word; Shift: TShiftState );
    procedure KeyUp; overload;
    procedure KeyUp( var Key: Word; Shift: TShiftState ); overload;
    procedure MouseDown( Button: TMouseButton; Shift: TShiftState; X, Y: Integer );
    procedure MouseLeave;
    procedure MouseMove( Shift: TShiftState; X, Y: Integer );
    procedure MouseUp( Button: TMouseButton; Shift: TShiftState; X, Y: Integer ); overload;
    procedure MouseUp; overload;
    { Properties }
    property AsString: WideString read GetAsString write SetAsString;
    property Control: TWinControl read GetControl write SetControl;
    property Font: TFont read GetFont write SetFont;
  end;

  { IValidatable }

  IValidatable = interface
    ['{9664A6C9-5351-4F01-9B33-F5FFC2F2F506}']
    function Validate( const Func: TFunc<boolean> ): TValidationResult;
  end;

  { IValidator }

  IValidator = interface
    ['{E8280A04-1A31-4721-83E9-0502DAFB3FB5}']
    function Validate( const Validatable: IValidatable ): TValidationResult;
    function GetErrorCount: Integer;
    property ErrorCount: Integer read GetErrorCount;
  end;

  { INxPreviewDrawable }

  TPreviewDrawEvent = procedure( Sender: TObject; PreviewRect: TRect ) of object;

  IPreviewDrawable = interface
    ['{7F413D49-FD91-4423-A698-9814C1E78C94}']
    { Property Accessors }
    function GetOnPreviewDraw: TPreviewDrawEvent;
    procedure SetOnPreviewDraw( const Value: TPreviewDrawEvent );
    { Properties }
    property OnPreviewDraw: TPreviewDrawEvent read GetOnPreviewDraw write SetOnPreviewDraw;
  end;

  { IPopupControl }

  IPopupControl = interface
    ['{B7D52F81-F7E6-47C5-8D25-4FB0746B6A50}']
    function GetCanvas: TCanvas;
    function GetFont: TFont;
    procedure KeyDown( var Key: Word; Shift: TShiftState );
    procedure Popup( const X, Y: Integer; Anchor: TPopupAnchor = paLeft ); overload;
    procedure Popup( const Location: TPoint; Anchor: TPopupAnchor = paLeft ); overload;
    procedure SetDropDown( const Value: IDropDown );
    procedure UpdatePopup;
    { Properties }
    property Canvas: TCanvas read GetCanvas;
    property Font: TFont read GetFont;
  end;

  { IOwnerDrawn }

  IOwnerDrawn = interface
    ['{3C7387EA-640C-4DF6-8F9F-1D3EACF033FA}']
    function GetDrawingOptions: TNxDrawingOptions;
    property DrawingOptions: TNxDrawingOptions read GetDrawingOptions;
  end;

  { ITextControl }

  ITextControl = interface
    ['{EC1B20D0-D182-43A8-BEBA-22338F85E90A}']
    function GetText: TCaption;
    property Text: TCaption read GetText;
  end;

  { IButtonEdit }

  IButtonEdit = interface
    ['{72798DF5-D921-41E0-856D-AB9FF32BC33B}']
    procedure SetOnButtonClick( const Value: TNotifyEvent );
  end;

  { INxInplaceEdit }

  INxInplaceEdit = interface
    ['{58A54A82-1A64-4225-BA8C-84ECD56A0417}']
    { Property Accessors }
    function GetOnChange: TNotifyEvent;
    function GetShowing: Boolean;
    function GetText: TCaption;
    procedure SetAlignment( const Value: TAlignment );
    procedure SetAutoSelect( const Value: Boolean );
    procedure SetEditOptions( const Value: TEditOptions );
    procedure SetOnChange( const Value: TNotifyEvent );
    procedure SetReadOnly( const Value: Boolean );
    procedure SetText( const Value: TCaption );
    procedure SetWantTabs( const Value: Boolean );
    { TWinControl Private }
    function GetHandle: HWND;
    procedure SetColor( const Value: TColor );
    procedure SetFont( const Value: TFont );
    { TWinControl Protected }
    procedure SetEnabled( Value: Boolean );
    procedure SetParent( AParent: TWinControl );
    { Methods }
    procedure Assign( Source: TPersistent );
    procedure AssignValue( Source: IValueAccessors );
    procedure BeginEdit; overload;
    procedure BeginEdit( Key: WideChar ); overload;
    function CanDispatchKey( const Key: Word ): Boolean;
    procedure EndEdit;
    function Focused: Boolean;
    procedure Free;
    procedure Hide;
    procedure SelectAll;
    procedure SetEditPadding( const Leading, Trailing: Integer );
    procedure SetBounds( ALeft, ATop, AWidth, AHeight: Integer );
    procedure SetFocus;
    procedure Show;
    { Properties }
    property Alignment: TAlignment write SetAlignment;
    property AutoSelect: Boolean write SetAutoSelect;
    property Color: TColor write SetColor;
    property EditOptions: TEditOptions write SetEditOptions;
    property Enabled: Boolean write SetEnabled;
    property Font: TFont write SetFont;
    property Handle: HWND read GetHandle;
    property Parent: TWinControl write SetParent;
    property readonly: Boolean write SetReadOnly;
    property Showing: Boolean read GetShowing;
    property Text: TCaption read GetText write SetText;
    property WantTabs: Boolean write SetWantTabs;
    property OnChange: TNotifyEvent read GetOnChange write SetOnChange;
  end;

implementation

end.
