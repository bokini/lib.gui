unit Unit1;

interface

uses
  Winapi.Windows, Winapi.Messages, System.SysUtils, System.Variants, System.Classes, Vcl.Graphics,
  Vcl.Controls, Vcl.Forms, Vcl.Dialogs, Vcl.StdCtrls,
  DIPS.FastTrak.UI.Edit, DIPS.FastTrak.UI.Controls, DIPS.FastTrak.UI.Types,
  DIPS.FastTrak.UI.DatePicker, DIPS.FastTrak.UI.Buttons;

type
  TForm1 = class( TForm )
    dcEdit1: TdcEdit;
    dcDatePicker1: TdcDatePicker;
    dcCheckBox1: TdcCheckBox;
    dcSpinEdit1: TdcSpinEdit;
    dcSpinEdit2: TdcSpinEdit;
    dcSpinEdit3: TdcSpinEdit;
    dcSpinEdit4: TdcSpinEdit;
    dcSpinEdit5: TdcSpinEdit;
    Button1: TButton;
    dcEdit2: TdcEdit;
    dcComboBox1: TdcComboBox;
    dcExecuteButton1: TdcExecuteButton;
    dcButtonEdit1: TdcButtonEdit;
    dcSpinEdit6: TdcSpinEdit;
    procedure Button1Click( Sender: TObject );
  private
    { Private declarations }
  public
    { Public declarations }
  end;

var
  Form1: TForm1;

implementation

{$R *.dfm}

procedure TForm1.Button1Click( Sender: TObject );
begin
  dcSpinEdit1.ValidationResult := vrInvalid;
  dcSpinEdit2.ValidationResult := vrInvalid;
  dcSpinEdit3.ValidationResult := vrInvalid;
  dcSpinEdit4.ValidationResult := vrInvalid;
  dcSpinEdit5.ValidationResult := vrInvalid;
  dcEdit2.Validate(
    function: boolean
    begin
      Result := false;
    end );
end;

end.
