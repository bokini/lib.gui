{ ------------------------------------------------------------- }
{ Purpose : DIPS FastTrak UI }
{ By      : Bojan Nikolic }
{ For     : DIPS AS }
{ ------------------------------------------------------------- }
{ Copyright (C) DIPS AS 2020. All Rights Reserved. }
{ ------------------------------------------------------------- }
unit DIPS.FastTrak.UI.Buttons;

interface

uses
  System.Classes, System.Types,
  {Winapi}
  Winapi.Windows, Winapi.Messages,
  {VCL}
  Vcl.Controls, Vcl.Graphics,
  {DIPS}
  DIPS.FastTrak.UI.Consts,
  DIPS.FastTrak.UI.Types,
  DIPS.FastTrak.UI.Intf;

type
  { TdcGraphicButton }

  TdcGraphicButton = class( TGraphicControl )
  private
    fState: TButtonState;
    fDefault: Boolean;
    procedure SetState( const Value: TButtonState );
    procedure SetDefault( const Value: Boolean );
  protected
    { Delphi Messages }
    procedure CMMouseEnter( var Message: TMessage ); message CM_MOUSEENTER;
    procedure CMMouseLeave( var Message: TMessage ); message CM_MOUSELEAVE;
    procedure WMEraseBkgnd( var Message: TWmEraseBkgnd ); message WM_ERASEBKGND;
    { Virtual Methods }
    procedure MouseDown( Button: TMouseButton; Shift: TShiftState; X, Y: Integer ); override;
    procedure MouseUp( Button: TMouseButton; Shift: TShiftState; X, Y: Integer ); override;
    procedure Paint; override;
    procedure WndProc( var Message: TMessage ); override;
    { Properties }
    property State: TButtonState read fState write SetState;
  public
    constructor Create( AOwner: TComponent ); override;
  published
    { Properties }
    property default: Boolean read fDefault write SetDefault default false;
    property Caption;
    property Font;
    property OnClick;
  end;

  { TdcEditButton }

  IEditButton = interface
    ['{57DD4C89-D01E-45FD-A9DB-EAD364587D52}']
    { Property Accessors }
    function GetAlign: TAlign;
    function GetAutoUp: Boolean;
    function GetCaption: WideString;
    function GetDown: Boolean;
    function GetEdit: INxInplaceEdit;
    function GetHandle: HWND;
    function GetHover: Boolean;
    function GetLeft: Integer;
    function GetOnButtonClick: TNotifyEvent;
    function GetOnDown: TNotifyEvent;
    function GetOnUp: TNotifyEvent;
    function GetParent: TWinControl;
    function GetVisible: Boolean;
    function GetWidth: Integer;
    procedure SetAlign( const Value: TAlign );
    procedure SetAutoUp( const Value: Boolean );
    procedure SetCaption( const Value: WideString );
    procedure SetDown( const Value: Boolean );
    procedure SetEdit( const Value: INxInplaceEdit );
    procedure SetHover( const Value: Boolean );
    procedure SetLeft( const Value: Integer );
    procedure SetOnButtonClick( const Value: TNotifyEvent );
    procedure SetOnDown( const Value: TNotifyEvent );
    procedure SetOnUp( const Value: TNotifyEvent );
    procedure SetParent( AParent: TWinControl );
    procedure SetVisible( const Value: Boolean );
    procedure SetWidth( const Value: Integer );
    { Methods }
    procedure Invalidate;
    { Properties }
    property Align: TAlign read GetAlign write SetAlign;
    property AutoUp: Boolean read GetAutoUp write SetAutoUp;
    property Caption: WideString read GetCaption write SetCaption;
    property Down: Boolean read GetDown write SetDown;
    property Edit: INxInplaceEdit read GetEdit write SetEdit;
    property Handle: HWND read GetHandle;
    property Hover: Boolean read GetHover write SetHover;
    property Left: Integer read GetLeft write SetLeft;
    property Parent: TWinControl read GetParent write SetParent;
    property Visible: Boolean read GetVisible write SetVisible;
    property Width: Integer read GetWidth write SetWidth;
    { Events }
    property OnButtonClick: TNotifyEvent read GetOnButtonClick write SetOnButtonClick;
    property OnDown: TNotifyEvent read GetOnDown write SetOnDown;
    property OnUp: TNotifyEvent read GetOnUp write SetOnUp;
  end;

  { TdcEditButton }

  TdcEditButton = class( TCustomControl, IEditButton )
  private
    { Property Fields }
    FAutoUp: Boolean;
    FCaption: WideString;
    FDown: Boolean;
    FEdit: INxInplaceEdit;
    FHover: Boolean;
    FMouseDown: Boolean;
    FOnButtonClick: TNotifyEvent;
    FOnDown: TNotifyEvent;
    FOnUp: TNotifyEvent;
    { Property Accessors }
    function GetAlign: TAlign;
    function GetAutoUp: Boolean;
    function GetCaption: WideString;
    function GetDown: Boolean;
    function GetEdit: INxInplaceEdit;
    function GetHandle: HWND;
    function GetHover: Boolean;
    function GetLeft: Integer;
    function GetOnButtonClick: TNotifyEvent;
    function GetOnDown: TNotifyEvent;
    function GetOnUp: TNotifyEvent;
    function GetParent: TWinControl;
    function GetVisible: Boolean;
    function GetWidth: Integer;
    procedure SetAlign( const Value: TAlign );
    procedure SetAutoUp( const Value: Boolean );
    procedure SetCaption( const Value: WideString );
    procedure SetDown( const Value: Boolean );
    procedure SetEdit( const Value: INxInplaceEdit );
    procedure SetHover( const Value: Boolean );
    procedure SetLeft( const Value: Integer );
    procedure SetOnButtonClick( const Value: TNotifyEvent );
    procedure SetOnDown( const Value: TNotifyEvent );
    procedure SetOnUp( const Value: TNotifyEvent );
    procedure SetVisible( const Value: Boolean );
    procedure SetWidth( const Value: Integer );
  protected
    procedure DoButtonClick; dynamic;
    procedure DoDown; dynamic;
    procedure DoUp; dynamic;
    { Mouse & Keyboard Actions }
    procedure MouseDown( Button: TMouseButton; Shift: TShiftState; X, Y: Integer ); override;
    procedure MouseUp( Button: TMouseButton; Shift: TShiftState; X, Y: Integer ); override;
    { Painting }
    procedure Paint; override;
    procedure PaintBackground; virtual; abstract;
    { Virtual Methods }
    procedure SetParent( AParent: TWinControl ); override;
    { Delphi Messages }
    procedure CMMouseLeave( var Message: TMessage ); message CM_MOUSELEAVE;
    procedure CMMouseEnter( var Message: TMessage ); message CM_MOUSEENTER;
    { Windows Messages }
    procedure WMEraseBkgnd( var Message: TWmEraseBkgnd ); message WM_ERASEBKGND;
  public
    constructor Create( AOwner: TComponent ); override;
    { Properties }
    property AutoUp: Boolean read GetAutoUp write SetAutoUp;
    property Down: Boolean read GetDown write SetDown;
    property Edit: INxInplaceEdit read GetEdit write SetEdit;
    property Handle: HWND read GetHandle;
    property Hover: Boolean read GetHover write SetHover;
    property Parent: TWinControl read GetParent write SetParent;
    { Events }
    property OnButtonClick: TNotifyEvent read GetOnButtonClick write SetOnButtonClick;
    property OnDown: TNotifyEvent read GetOnDown write SetOnDown;
    property OnUp: TNotifyEvent read GetOnUp write SetOnUp;
  published
    { Properties }
    property Align;
    property Anchors;
    property Caption: WideString read GetCaption write SetCaption;
    property Color;
    property Constraints;
    property Cursor default crHandPoint;
    property Visible: Boolean read GetVisible write SetVisible;
    property Width: Integer read GetWidth write SetWidth;
    { Events }
    property OnClick;
    property OnContextPopup;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnStartDock;
    property OnStartDrag;
  end;

  { TdcDropDownButton }

  TdcDropDownButton = class( TdcEditButton )
  protected
    procedure PaintBackground; override;
  public
    constructor Create( AOwner: TComponent ); override;
  end;

  { TdcExecuteButton }

  IExecuteButton = interface
    ['{92CF3F17-0593-46AD-9225-324BFA4CDBD1}']
    procedure SetCloseGlyph( const Value: Boolean );
    procedure SetGlyph( const Value: TPicture );
  end;

  TdcExecuteButton = class( TdcEditButton, IExecuteButton )
  private
    fGlyph: TPicture;
    fCloseGlyph: Boolean;
    procedure SetGlyph( const Value: TPicture );
    procedure SetCloseGlyph( const Value: Boolean );
  protected
    procedure DoGlyphChange( Sender: TObject );
    procedure Paint; override;
    procedure PaintBackground; override;
  public
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    class procedure DrawCloseSign( Canvas: TCanvas; const X, Y: Integer );
    class procedure PaintInRect( Canvas: TCanvas; Dest: TRect; CloseGlyph: Boolean; Glyph: TPicture; Text: WideString );
  published
    { Properties }
    property CloseGlyph: Boolean read fCloseGlyph write SetCloseGlyph;
    property Glyph: TPicture read fGlyph write SetGlyph;
  end;

implementation

uses
  System.SysUtils,
  DIPS.FastTrak.UI.Helpers;

{ TdcGraphicButton }

procedure TdcGraphicButton.CMMouseEnter( var Message: TMessage );
begin
  inherited;
  State := State + [btHot];
end;

procedure TdcGraphicButton.CMMouseLeave( var Message: TMessage );
begin
  inherited;
  State := State - [btHot];
end;

constructor TdcGraphicButton.Create( AOwner: TComponent );
begin
  inherited;
  fDefault := false;
  ControlStyle := [csCaptureMouse, csDoubleClicks, csOpaque];
end;

procedure TdcGraphicButton.MouseDown( Button: TMouseButton; Shift: TShiftState; X, Y: Integer );
begin
  inherited;
  State := State + [btPushed];
end;

procedure TdcGraphicButton.MouseUp( Button: TMouseButton; Shift: TShiftState; X, Y: Integer );
begin
  inherited;
  if btPushed in State then
  begin
    State := State - [btPushed];
    Click;
  end;
end;

procedure TdcGraphicButton.Paint;
var
  FaceColor: TColor;
begin
  inherited;
  FaceColor := ButtonColor[fDefault];
  if btHot in fState then
  begin
    FaceColor := ButtonColorHot[fDefault];
    if btPushed in fState then
      FaceColor := ButtonColorPressed[fDefault];
  end;
  Canvas.Brush.Color := FaceColor;
  Canvas.FillRect( ClientRect );
  Canvas.Font.Color := ButtonColorText[fDefault];
  Canvas.RenderText( ClientRect, Caption, [tfCenter, tfSingleLine, tfVerticalCenter] );
end;

procedure TdcGraphicButton.SetDefault( const Value: Boolean );
begin
  if Value <> fDefault then
  begin
    fDefault := Value;
    Invalidate;
  end;
end;

procedure TdcGraphicButton.SetState( const Value: TButtonState );
begin
  if Value <> fState then
  begin
    fState := Value;
    Invalidate;
  end;
end;

procedure TdcGraphicButton.WMEraseBkgnd( var Message: TWmEraseBkgnd );
begin
  message.Result := 1;
end;

procedure TdcGraphicButton.WndProc( var Message: TMessage );
begin
  if Owner <> nil then
    inherited;
end;

{ TdcEditButton }

procedure TdcEditButton.CMMouseEnter( var Message: TMessage );
begin
  if not( csDesigning in ComponentState ) then
  begin
    Hover := True;
  end;
end;

procedure TdcEditButton.CMMouseLeave( var Message: TMessage );
begin
  if not( csDesigning in ComponentState ) then
  begin
    Hover := false;
  end;
end;

constructor TdcEditButton.Create( AOwner: TComponent );
begin
  inherited;
  Supports( AOwner, INxInplaceEdit, FEdit );
  FAutoUp := True;
  FDown := false;
  FHover := false;
  FMouseDown := false;
  Cursor := crHandPoint;
end;

procedure TdcEditButton.DoButtonClick;
begin
  if Assigned( OnButtonClick ) then
    FOnButtonClick( Self );
end;

procedure TdcEditButton.DoDown;
begin
  if Assigned( OnDown ) then
    FOnDown( Self );
end;

procedure TdcEditButton.DoUp;
begin
  if Assigned( FOnUp ) then
    FOnUp( Self );
end;

function TdcEditButton.GetAlign: TAlign;
begin
  Result := inherited Align;
end;

function TdcEditButton.GetAutoUp: Boolean;
begin
  Result := FAutoUp;
end;

function TdcEditButton.GetCaption: WideString;
begin
  Result := FCaption;
end;

function TdcEditButton.GetDown: Boolean;
begin
  Result := FDown;
end;

function TdcEditButton.GetEdit: INxInplaceEdit;
begin
  Result := FEdit;
end;

function TdcEditButton.GetHandle: HWND;
begin
  Result := inherited Handle;
end;

function TdcEditButton.GetHover: Boolean;
begin
  Result := FHover;
end;

function TdcEditButton.GetLeft: Integer;
begin
  Result := inherited Left;
end;

function TdcEditButton.GetOnButtonClick: TNotifyEvent;
begin
  Result := FOnButtonClick;
end;

function TdcEditButton.GetOnDown: TNotifyEvent;
begin
  Result := FOnDown;
end;

function TdcEditButton.GetOnUp: TNotifyEvent;
begin
  Result := FOnUp;
end;

function TdcEditButton.GetParent: TWinControl;
begin
  Result := inherited Parent;
end;

function TdcEditButton.GetVisible: Boolean;
begin
  Result := inherited Visible;
end;

function TdcEditButton.GetWidth: Integer;
begin
  Result := inherited Width;
end;

procedure TdcEditButton.MouseDown( Button: TMouseButton; Shift: TShiftState; X, Y: Integer );
begin
  inherited;
  Down := not Down;

  { Used in MouseUp }
  FMouseDown := True;

  { Focus Edit too! }
  if Assigned( FEdit ) and FEdit.Showing then
  begin
    FEdit.SetFocus;
  end;
end;

procedure TdcEditButton.MouseUp( Button: TMouseButton; Shift: TShiftState; X, Y: Integer );
begin
  inherited;
  if FAutoUp then
    Down := false;

  { Is Click? }
  if FMouseDown then
  begin
    FMouseDown := false;

    { Trigger Event }
    DoButtonClick;
  end;
end;

procedure TdcEditButton.Paint;
begin
  inherited;
  PaintBackground;
end;

procedure TdcEditButton.SetAlign( const Value: TAlign );
begin
  inherited Align := Value;
end;

procedure TdcEditButton.SetAutoUp( const Value: Boolean );
begin
  FAutoUp := Value;
end;

procedure TdcEditButton.SetCaption( const Value: WideString );
begin
  FCaption := Value;
  Invalidate;
end;

procedure TdcEditButton.SetDown( const Value: Boolean );
begin
  if Value <> FDown then
  begin
    FDown := Value;
    Refresh;
    if not FDown then
      DoUp
    else
      DoDown;
  end;
end;

procedure TdcEditButton.SetEdit( const Value: INxInplaceEdit );
begin
  FEdit := Value;
end;

procedure TdcEditButton.SetHover( const Value: Boolean );
begin
  if Value <> FHover then
  begin
    FHover := Value;
    Invalidate;
  end;
end;

procedure TdcEditButton.SetLeft( const Value: Integer );
begin
  inherited Left := Value;
end;

procedure TdcEditButton.SetOnButtonClick( const Value: TNotifyEvent );
begin
  FOnButtonClick := Value;
end;

procedure TdcEditButton.SetOnDown( const Value: TNotifyEvent );
begin
  FOnDown := Value;
end;

procedure TdcEditButton.SetOnUp( const Value: TNotifyEvent );
begin
  FOnUp := Value;
end;

procedure TdcEditButton.SetParent( AParent: TWinControl );
begin
  inherited SetParent( AParent );
end;

procedure TdcEditButton.SetVisible( const Value: Boolean );
begin
  inherited Visible := Value;
end;

procedure TdcEditButton.SetWidth( const Value: Integer );
begin
  inherited Width := Value;
end;

procedure TdcEditButton.WMEraseBkgnd( var Message: TWmEraseBkgnd );
begin
  message.Result := 1;
end;

{ TNxDropDownButton }

constructor TdcDropDownButton.Create( AOwner: TComponent );
begin
  inherited;
  Height := 19;
  Width := DropDownButtonWidth;
end;

procedure TdcDropDownButton.PaintBackground;
var
  arrowRect: TRect;
  buttonState: TButtonState;
  buttonFace: TColor;

  procedure DrawDropDownArrow( AArrowRect: TRect );
  begin
    with AArrowRect do
    begin
      Canvas.Pen.Color := clGlyph;
      Canvas.Brush.Color := clGlyph;
      Canvas.Polygon( [Point( Left, Top ), Point( Right, Top ), Point( Left + Width div 2, Bottom - 1 )] );
    end;
  end;

begin
  if csDesigning in ComponentState then
    buttonFace := clBtnFaceDropDown
  else
  begin
    buttonState := [];
    if Hover then
      Include( buttonState, btHot );
    if Down then
      Include( buttonState, btPushed );
    if not Parent.Enabled then
      Include( buttonState, btPushed );

    buttonFace := clEditControl;
    if btHot in buttonState then
    begin
      buttonFace := clBtnFaceDropDown;
      if btPushed in buttonState then
        buttonFace := clBtnFaceNormalPressed;
    end;
  end;

  with Canvas do
  begin
    Brush.Color := buttonFace;
    FillRect( ClientRect );
    arrowRect := CenteredRect( ClientRect, Bounds( 0, 0, 8, 5 ) );
    DrawDropDownArrow( arrowRect );
  end;
end;

{ TdcExecuteButton }

constructor TdcExecuteButton.Create( AOwner: TComponent );
begin
  inherited;
  fCloseGlyph := false;
  fGlyph := TPicture.Create;
  fGlyph.OnChange := DoGlyphChange;
  Width := ExecuteButtonWidth;
  Height := 21;
end;

destructor TdcExecuteButton.Destroy;
begin
  FreeAndNil( fGlyph );
  inherited;
end;

procedure TdcExecuteButton.DoGlyphChange( Sender: TObject );
begin
  Invalidate;
end;

class procedure TdcExecuteButton.DrawCloseSign( Canvas: TCanvas; const X, Y: Integer );
begin
  with Canvas do
  begin
    Polygon( [Point( X, Y ), Point( X + 8, Y + 8 ), Point( X + 9, Y + 8 ), Point( X + 1, Y + 0 ), Point( X, Y )] );
    Polygon( [Point( X, Y + 8 ), Point( X + 8, Y ), Point( X + 9, Y ), Point( X + 1, Y + 8 ), Point( X,
     Y + 8 )] );
  end;
end;

procedure TdcExecuteButton.Paint;
begin
  inherited;
  PaintInRect( Canvas, ClientRect, CloseGlyph, Glyph, Caption );
end;

procedure TdcExecuteButton.PaintBackground;
var
  buttonState: TButtonState;
  buttonFace: TColor;
begin
  if csDesigning in ComponentState then
    buttonFace := clBtnFaceDropDown
  else
  begin
    buttonState := [];
    if Hover then
      Include( buttonState, btHot );
    if Down then
      Include( buttonState, btPushed );
    if not Parent.Enabled then
      Include( buttonState, btDisabled );

    buttonFace := clEditControl;
    if btHot in buttonState then
    begin
      buttonFace := clBtnFaceDropDown;
      if btPushed in buttonState then
        buttonFace := clBtnFaceNormalPressed;
    end;
  end;

  with Canvas do
  begin
    Brush.Color := buttonFace;
    FillRect( ClientRect );
  end;
end;

class procedure TdcExecuteButton.PaintInRect( Canvas: TCanvas; Dest: TRect; CloseGlyph: Boolean; Glyph: TPicture; Text: WideString );
var
  X, Y: Integer;
  bitmap: TBitmap;
  textStr: string;
  glyphRect, textRect: TRect;
begin
  if CloseGlyph then
  begin
    glyphRect := TRect.Create( 0, 0, 8, 8 );
    glyphRect := CenteredRect( Dest, glyphRect );
    DrawCloseSign( Canvas, glyphRect.Left, glyphRect.Top );
  end
  else
  begin
    if Assigned( Glyph ) and Assigned( Glyph.Graphic ) then
    begin
      glyphRect := TRect.Create( 0, 0, Glyph.Width, Glyph.Height );
      glyphRect := CenteredRect( Dest, glyphRect );
      if Glyph.Graphic is TBitmap then
      begin
        bitmap := Glyph.Graphic as TBitmap;
        bitmap.Transparent := True;
        bitmap.TransparentColor := bitmap.Canvas.Pixels[0, Pred( bitmap.Height )];
      end;
      Canvas.Draw( glyphRect.Left, glyphRect.Top, Glyph.Graphic );
    end;
  end;

  if Text <> '' then
  begin
    textRect := Dest;
    textStr := Text;
    Canvas.textRect( textRect, textStr, [tfCenter, tfEndEllipsis, tfSingleLine, tfVerticalCenter] );
  end;
end;

procedure TdcExecuteButton.SetGlyph( const Value: TPicture );
begin
  fGlyph.Assign( Value );
  if Assigned( fGlyph ) then
    fGlyph.OnChange := DoGlyphChange;
end;

procedure TdcExecuteButton.SetCloseGlyph( const Value: Boolean );
begin
  if Value <> fCloseGlyph then
  begin
    fCloseGlyph := Value;
    Invalidate;
  end;
end;

end.
