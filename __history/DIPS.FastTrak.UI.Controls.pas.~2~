unit DIPS.FastTrak.UI.Controls;

interface

uses
  { Standard }
  Classes, Types, Messages, Windows, Graphics, ExtCtrls,
  { Controls }
  Controls, StdCtrls, Forms,
  UITypes,
  {DIPS}
  DIPS.FastTrak.UI.Controls.Types,
  DIPS.FastTrak.UI.Intf;

type
  { INxScrollingControl }
  TNxScrollBar = class;

  TNxScrollType = (stFirst, stLast, stSmallDecrement, stSmallIncrement,
    stLargeDecrement, stLargeIncrement, stThumbPosition, stThumbTrack, stEndScroll);

  INxScrollingControl = interface
    ['{3D0B2919-E99B-48DA-9F18-6A32D3DB15E0}']
    function GetEnabled: Boolean;
    function GetHandle: HWND;
    function GetScrollBars: TScrollStyle;
    function GetVertScrollBar: TNxScrollBar;
    function HandleAllocated: Boolean;
    function IsDestroying: Boolean;
    function IsReading: Boolean;
    procedure ScrollContentBy(DeltaX, DeltaY: Integer);
    procedure ScrollRect(DeltaX, DeltaY: Integer; Rect, ClipRect: TRect);
    { Properties }
    property Enabled: Boolean read GetEnabled;
    property Handle: HWND read GetHandle;
    property ScrollBars: TScrollStyle read GetScrollBars;
    property VertScrollBar: TNxScrollBar read GetVertScrollBar;
  end;

  { TNxUserControl6 }

  TNxPaintState = set of (ptPainting);

  TNxUserControl6 = class(TCustomControl, IViewOwner)
  private
    function GetPaddingRect: TRect;
  protected
    FPaintingState: TNxPaintState;
    { INxViewOwner }
    function GetCanvas: TCanvas;
    function GetHandle: THandle;
  protected
    function CanFocusParent: Boolean;
    procedure DoPaddingChange(Sender: TObject); virtual;
    procedure EraseRect(Rect: TRect);
    { Invalidation Methods }
    procedure ValidateRect(const Source: TRect);
    procedure InvalidateRect(const Source: TRect);
    { Messages }
    procedure WMEraseBkGnd(var Message: TWMEraseBkGnd); message WM_ERASEBKGND;
    { Properties }
    property PaddingRect: TRect read GetPaddingRect;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property Action;
    property Align;
    property Anchors;
    property Color;
    property Constraints;
    property DragCursor;
    property DragKind;
    property DragMode;
    property Enabled;
    property Font;
    property Hint;
    property Margins;
    property Padding;
    property ParentColor;
    property ParentFont;
    property ParentShowHint;
    property PopupMenu;
    property ShowHint;
    property TabOrder;
    property TabStop;
    property Tag;
    property Touch;
    property Visible;

    property OnClick;
    property OnContextPopup;
    property OnDblClick;
    property OnDragDrop;
    property OnDragOver;
    property OnEndDock;
    property OnEndDrag;
    property OnEnter;
    property OnExit;
    property OnGesture;
    property OnKeyDown;
    property OnKeyPress;
    property OnKeyUp;
    property OnMouseDown;
    property OnMouseMove;
    property OnMouseUp;
    property OnMouseEnter;
    property OnMouseLeave;
    property OnStartDock;
    property OnStartDrag;
  end;

  { TNxControl6 }

  TNxControl6 = class(TNxUserControl6)
  private
    { Property Fields }
    FBorderColor: TColor;
    FBorderSize: Integer;
    FBorderStyle: TNxBorderStyle;
    FHintLocation: TPoint;
    FHintPauseTimer: TTimer;
    FHintText: WideString;
    FHintWindow: THintWindow;
    FOnPaint: TNotifyEvent;
    FTagString: string;
    { Property Acessors }
    procedure SetBorderColor(const Value: TColor);
    procedure SetBorderSize(const Value: Integer);
    procedure SetBorderStyle(const Value: TNxBorderStyle);
  protected
    procedure ActivateHint(Location: TPoint; Text: WideString);
    procedure DeactivateHint;
    procedure DoHintPauseTimer(Sender: TObject);
    procedure DoPaint; dynamic;
    procedure EraseBkGnd(const Source: TRect; AColor: TColor = clNone);
    procedure PaintWindow(DC: HDC); override;
    { Delphi Messages }
    procedure CMMouseLeave(var Message: TMessage); message CM_MOUSELEAVE;
    { Win32 Messages }
    procedure WMNCCalcSize(var Message: TWMNCCalcSize); message WM_NCCALCSIZE;
    procedure WMNCPaint(var Message: TMessage); message WM_NCPAINT;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
  published
    property BorderColor: TColor read FBorderColor write SetBorderColor default clBtnShadow;
    property BorderSize: Integer read FBorderSize write SetBorderSize default 0;
    property BorderStyle: TNxBorderStyle read FBorderStyle write SetBorderStyle default btSolid;
    property TagString: string read FTagString write FTagString;
    property OnPaint: TNotifyEvent read FOnPaint write FOnPaint;
  end;

  { TNxCustomScrollBar }

  INxScrollBar = interface
    ['{FFA366CC-2DEB-4454-B19F-5E24B559491A}']
    { Property Accessors }
    function GetPosition: Integer;
    function GetShowing: Boolean;
    procedure SetPosition(const Value: Integer);
    { Methods }
    procedure Clear(Update: Boolean = False);
    procedure First;
    procedure Hide;
    function IsFirst: Boolean;
    function IsLast: Boolean;
    procedure Last;
    procedure Lock;
    procedure MoveBy(Distance: Integer);
    procedure Next;
    procedure PageDown;
    procedure PageUp;
    procedure Prior;
    procedure Scroll(ScrollType: TNxScrollType);
    procedure SetValues(Max, PageSize: Integer);
    procedure Show;
    procedure Unlock(Update: Boolean = True);
    procedure Update;
    { Properties }
    property Position: Integer read GetPosition write SetPosition;
  end;

  { TNxPersistentScrollBar6 }

  TNxPersistentScrollBar6 = class(TInterfacedPersistent)
  private
    FLargeChange: Integer;
    FMax: Integer;
    FOnChange: TNotifyEvent;
    FPageSize: Integer;
    FPosition: Integer;
    FSmallChange: Integer;
    FVisible: Boolean;
    function GetLargeChange: Integer;
    function GetMax: Integer;
    function GetPageSize: Integer;
    function GetPosition: Integer;
    function GetSmallChange: Integer;
    function GetVisible: Boolean;
    procedure SetLargeChange(const Value: Integer);
    procedure SetMax(const Value: Integer);
    procedure SetPageSize(const Value: Integer);
    procedure SetPosition(const Value: Integer);
    procedure SetSmallChange(const Value: Integer);
    procedure SetVisible(const Value: Boolean);
  protected
    procedure DoChange; dynamic;
  public
    constructor Create; virtual;
    procedure PageUp;
    procedure PageDown;
    procedure Prior;
    procedure Next;
  published
    property LargeChange: Integer read GetLargeChange write SetLargeChange;
    property Max: Integer read GetMax write SetMax;
    property Position: Integer read GetPosition write SetPosition;
    property PageSize: Integer read GetPageSize write SetPageSize;
    property SmallChange: Integer read GetSmallChange write SetSmallChange;
    property Visible: Boolean read GetVisible write SetVisible;
    property OnChange: TNotifyEvent read FOnChange write FOnChange;
  end;

  TNxCustomScrollBar = class(TInterfacedPersistent, INxScrollBar)
  private
    { Property Fields }
    FAutoHide: Boolean;
    FControl: INxScrollingControl;
    FManualScroll: Boolean;
    FEnabled: Boolean;
    FKind: TScrollBarKind;
    FLargeChange: Integer;
    FLocked: Boolean;
    FMax: Integer;
    FMin: Integer;
    FPageSize: Integer;
    FPosition: Integer;
    FSmallChange: Integer;
    FSnapshotPosition: Integer;
    FScrollKind: TScrollBarKind;
    FUpdating: Boolean;
    FVisible: Boolean;
    { Property Accessors }
    function GetInfoFlag: Cardinal;
    function GetScrollInfo: TScrollInfo;
    function GetShowing: Boolean;
    function GetSnapshotPosition: Integer;
    function GetThumbPosition: Integer;
    procedure SetAutoHide(const Value: Boolean);
    procedure SetEnabled(const Value: Boolean);
    procedure SetKind(const Value: TScrollBarKind);
    procedure SetManualScroll(const Value: Boolean);
    procedure SetMax(const Value: Integer);
    procedure SetMin(const Value: Integer);
    procedure SetPageSize(const Value: Integer);
    procedure SetPosition(const Value: Integer);
  protected
    procedure CheckValues;
    function GetControlScrollBars: TScrollStyle;
    function GetFlag: Integer; virtual;
    procedure SetVisible(const Value: Boolean); virtual;
    function ShouldBeVisible: Boolean;
    { Virtual Methods }
    function GetPosition: Integer; virtual;
    procedure UpdateScrollBar; virtual;
    { Properties }
    property Flag: Integer read GetFlag;
    property InfoFlag: Cardinal read GetInfoFlag;
    property ScrollInfo: TScrollInfo read GetScrollInfo;
  public
    constructor Create(AControl: INxScrollingControl; AKind: TScrollBarKind); virtual;
    destructor Destroy; override;
    function IsFirst: Boolean;
    function IsLast: Boolean;
    procedure Assign(Source: TPersistent); override;
    procedure Clear(Update: Boolean = False);
    procedure EraseSnapshot;
    function IsUpdating: Boolean;
    procedure First;
    procedure Hide;
    procedure Last;
    procedure Lock;
    procedure MoveBy(Distance: Integer);
    procedure Next;
    procedure Prior;
    procedure PageDown; virtual;
    procedure PageUp; virtual;
    procedure SetValues(AMax, APageSize: Integer); virtual;
    procedure Scroll(ScrollType: TNxScrollType); virtual;
    procedure Show;
    procedure Snapshot;
    procedure Unlock(Update: Boolean = True);
    procedure Update; virtual;
    { Properties }
    property AutoHide: Boolean read FAutoHide write SetAutoHide;
    property Enabled: Boolean read FEnabled write SetEnabled;
    property Kind: TScrollBarKind read FKind write SetKind;
    property LargeChange: Integer read FLargeChange write FLargeChange;
    property ManualScroll: Boolean read FManualScroll write SetManualScroll;
    property Max: Integer read FMax write SetMax;
    property Min: Integer read FMin write SetMin;
    property PageSize: Integer read FPageSize write SetPageSize;
    property Position: Integer read GetPosition write SetPosition;
    property ScrollKind: TScrollBarKind read FScrollKind;
    property Showing: Boolean read GetShowing;
    property SmallChange: Integer read FSmallChange write FSmallChange;
    property SnapshotPosition: Integer read GetSnapshotPosition;
    property ThumbPosition: Integer read GetThumbPosition;
    property Visible: Boolean read FVisible write SetVisible;
  end;

  TNxScrollBar = class(TNxCustomScrollBar);

  { TNxCustomScrollControl6 }

  TNxScrollControl6 = class(TNxControl6, INxScrollingControl)
  private
    FHorzScrollBar: TNxScrollBar;
    FOnHorizontalScroll: TNotifyEvent;
    FOnContentScroll: TNotifyEvent;
    FOnVerticalScroll: TNotifyEvent;
    FScrollBars: TNxScrollBars;
    FVertScrollBar: TNxScrollBar;
    procedure SetScrollBars(const Value: TNxScrollBars);
  protected
    function GetHandle: HWND;
    function GetHorzScrollBar: TNxScrollBar;
    function GetScrollBars: TScrollStyle; virtual;
    function GetScrollType(ScrollCode: Integer): TNxScrollType;
    function GetVertScrollBar: TNxScrollBar;
    function IsDestroying: Boolean;
    function IsReading: Boolean;
    { Event Handlers }
    procedure DoHorizontalScroll; dynamic;
    procedure DoVerticalScroll; dynamic;
    { VCL Core }
    procedure CreateParams(var Params: TCreateParams); override;
    procedure CreateWnd; override;
    { Mouse Methods }
    function DoMouseWheelDown(Shift: TShiftState; MousePos: TPoint): Boolean; override;
    function DoMouseWheelUp(Shift: TShiftState; MousePos: TPoint): Boolean; override;
    { Methods }
    procedure ScrollContentBy(DeltaX, DeltaY: Integer); virtual;
    procedure ScrollRect(DeltaX, DeltaY: Integer; Rect, ClipRect: TRect); virtual;
    procedure SelectNextControl;
    procedure SelectPrevControl;
    { Win32 Messages }
    procedure WMHScroll(var Message: TWMHScroll); message WM_HSCROLL;
    procedure WMVScroll(var Message: TWMVScroll); message WM_VSCROLL;
    { Properties }
    property ScrollBars: TNxScrollBars read FScrollBars write SetScrollBars;
  public
    constructor Create(AOwner: TComponent); override;
    destructor Destroy; override;
    { Properties }
    property HorzScrollBar: TNxScrollBar read GetHorzScrollBar;
    property VertScrollBar: TNxScrollBar read GetVertScrollBar;
  published
    property OnContentScroll: TNotifyEvent read FOnContentScroll write FOnContentScroll;
    property OnHorizontalScroll: TNotifyEvent read FOnHorizontalScroll write FOnHorizontalScroll;
    property OnVerticalScroll: TNotifyEvent read FOnVerticalScroll write FOnVerticalScroll;
  end;

  { TNxFlyoverControl6 }

  TNxFlyoverControl6 = class(TNxControl6)
  protected
    procedure CreateParams(var Params: TCreateParams); override;
  end;

  { TNxTextFitHintWindow }

  TNxTextFitHintWindow = class(THintWindow)
  protected
    procedure CreateParams(var Params: TCreateParams); override;
  public
    constructor Create(AOwner: TComponent); override;
  end;

  { TNxScrollBarView }

  TNxScrollBarView = class
  private
    FOwner: IViewOwner;
  public
    constructor Create(AOwner: IViewOwner); virtual;
    procedure MouseLeave; virtual; abstract;
  end;

{$IFNDEF DROP_SHADOW}
const
  {$EXTERNALSYM CS_DROPSHADOW}
  CS_DROPSHADOW = $20000;

  function CheckWin32Version(AMajor: Integer; AMinor: Integer = 0): Boolean;
{$ENDIF}

  procedure RemoveKeyMessage(Handle: HWND);
  procedure RemovePaintMessage(Handle: HWND);

  function GetTaskBarAlign: TAlign;
  function GetTaskBarSize: TSize;

implementation

uses
  { Standard }
  Dialogs, SysUtils, ShellApi, Math;

function GetTaskBarBounds: TRect;
begin
  GetWindowRect(FindWindow('Shell_TrayWnd', ''), Result );
end;

function GetTaskBarSize: TSize;
var
  TaskBarBounds : TRect;
begin
  TaskBarBounds := GetTaskBarBounds;
  with (TaskBarBounds) do
  begin
    Result.cx := Right - Abs(Left);
    Result.cy := Bottom - Abs(Top);
  end;
end;

function GetTaskBarAlign: TAlign;
var
  TaskBarBounds : TRect;
begin
  Result := alNone;

  if( FindWindow('Shell_TrayWnd', '') > 0 ) then
  begin
    TaskBarBounds := GetTaskBarBounds;

    with( TaskBarBounds ) do
    // At Left or at top of screen ?
    if( Left <= 0 ) and ( Top <= 0 ) then
     begin
      if( Bottom >= 480 ) then
       Result := alLeft
      else Result := alTop;
     end
    else begin
          if( Left <= 0 ) then
           Result :=alBottom
          else Result :=alRight;
         end;
  end;
end;

{$IFNDEF DROP_SHADOW}
function CheckWin32Version(AMajor: Integer; AMinor: Integer = 0): Boolean;
begin
  Result := (Win32MajorVersion > AMajor) or
            ((Win32MajorVersion = AMajor) and
             (Win32MinorVersion >= AMinor));
end;
{$ENDIF}

procedure RemoveKeyMessage(Handle: HWND);
var
  Msg: TMsg;
begin
  Msg.Message := 0;
  if PeekMessage(Msg, Handle, WM_KEYFIRST, WM_KEYLAST, PM_REMOVE)
    and (Msg.Message = WM_QUIT) then PostQuitMessage(Msg.wParam);
end;

procedure RemovePaintMessage(Handle: HWND);
var
  Msg: TMsg;
begin
  Msg.Message := 0;
  PeekMessage(Msg, Handle, WM_PAINT, WM_PAINT, PM_REMOVE)
end;

{ TNxUserControl6 }

function TNxUserControl6.CanFocusParent: Boolean;
var
  Form: TWinControl;
begin
  Form := GetParentForm(Self);
  Result := Assigned(Form) and Form.Showing;
end;

constructor TNxUserControl6.Create(AOwner: TComponent);
begin
  inherited;
  FPaintingState := [];
  Padding.OnChange := DoPaddingChange;
  ControlStyle := ControlStyle + [csGestures];
end;

destructor TNxUserControl6.Destroy;
begin
  inherited;
end;

procedure TNxUserControl6.DoPaddingChange(Sender: TObject);
begin
  Realign;
end;

procedure TNxUserControl6.EraseRect(Rect: TRect);
begin
  Canvas.Brush.Color := Color;
  Canvas.FillRect(Rect);
end;

function TNxUserControl6.GetCanvas: TCanvas;
begin
  Result := Canvas;
end;

function TNxUserControl6.GetHandle: THandle;
begin
  Result := Handle;
end;

function TNxUserControl6.GetPaddingRect: TRect;
begin
  Result := ClientRect;
  NxClasses6.SetPadding(Result, Padding);
end;

procedure TNxUserControl6.InvalidateRect(const Source: TRect);
begin
  if HandleAllocated and not(ptPainting in FPaintingState) then
{$IFDEF UNSAFE}
  Windows.InvalidateRect(Handle, @Source, False);
{$ELSE}
  Windows.InvalidateRect(Handle, Source, False);
{$ENDIF}
end;

procedure TNxUserControl6.ValidateRect(const Source: TRect);
begin
  if HandleAllocated then
{$IFDEF UNSAFE}
  Windows.ValidateRect(Handle, @Source);
{$ELSE}
  Windows.ValidateRect(Handle, Source);
{$ENDIF}
end;

procedure TNxUserControl6.WMEraseBkGnd(var Message: TWMEraseBkGnd);
begin
  Message.Result := 1;
end;

{ TNxControl6 }

procedure TNxControl6.ActivateHint(Location: TPoint; Text: WideString);
begin
  if Text = ''
    then Exit;

  FHintLocation := Location;
  FHintText := Text;

  { Should pause? }
  if not Assigned(FHintWindow) then
  begin
    FHintPauseTimer := TTimer.Create(Self);
    FHintPauseTimer.Interval := Application.HintPause;
    FHintPauseTimer.OnTimer := DoHintPauseTimer;
  end else
    DoHintPauseTimer(Self); { Call now }
end;

procedure TNxControl6.CMMouseLeave(var Message: TMessage);
begin
  inherited;
  DeactivateHint;
end;

constructor TNxControl6.Create(AOwner: TComponent);
begin
  inherited;
  FBorderColor := clBtnShadow;
  FBorderSize := 0;
  FBorderStyle := btSolid;
  FTagString := EmptyStr;
end;

procedure TNxControl6.DeactivateHint;
begin
  { Stop & Destroy }
  FreeAndNil(FHintPauseTimer);

  if Assigned(FHintWindow) then
  begin
    FHintWindow.ReleaseHandle;
    { Destroy }
    if Assigned(FHintWindow) then FreeAndNil(FHintWindow);
  end;
end;

destructor TNxControl6.Destroy;
begin
  { Destroy Obj. }
  DeactivateHint;
  inherited;
end;

procedure TNxControl6.DoHintPauseTimer(Sender: TObject);
var
  HintRect: TRect;
begin
  { Release previous? }
  DeactivateHint;

  { Create Hint Window }
  FHintWindow := HintWindowClass.Create(nil);
  FHintWindow.Color := clInfoBk;

  { Set Position & Activate }

  { Calculate Rect }
  HintRect := FHintWindow.CalcHintRect(Screen.Width, FHintText, nil);

  { Cordinates must be "Screen" }
  HintRect.TopLeft := ClientToScreen(FHintLocation);

  HintRect.Bottom := HintRect.Top + HintRect.Bottom;
  HintRect.Right := HintRect.Left + HintRect.Right;

  { Show Hint }
  FHintWindow.ActivateHint(HintRect, FHintText);
end;

procedure TNxControl6.DoPaint;
begin
  if Assigned(FOnPaint) then FOnPaint(Self);
end;

procedure TNxControl6.EraseBkGnd(const Source: TRect; AColor: TColor);
begin
  with Canvas do
  begin
    if AColor <> clNone then Brush.Color := AColor
      else Brush.Color := Color;
    FillRect(Source);
  end;
end;

procedure TNxControl6.PaintWindow(DC: HDC);
begin
  inherited;
  DoPaint;
end;

procedure TNxControl6.SetBorderColor(const Value: TColor);
begin
  if Value <> FBorderColor then
  begin
    FBorderColor := Value;
    Perform(CM_BORDERCHANGED, 0, 0);
  end;
end;

procedure TNxControl6.SetBorderSize(const Value: Integer);
begin
  if Value <> FBorderSize then
  begin
    FBorderSize := Value;
    Perform(CM_BORDERCHANGED, 0, 0);
  end;
end;

procedure TNxControl6.SetBorderStyle(const Value: TNxBorderStyle);
begin
  if Value <> FBorderStyle then
  begin
    FBorderStyle := Value;
    Perform(CM_BORDERCHANGED, 0, 0);
  end;
end;

procedure TNxControl6.WMNCCalcSize(var Message: TWMNCCalcSize);
var
  Params: PNCCalcSizeParams;
begin
  inherited;
  Params := Message.CalcSize_Params;
  with Params^ do
  begin
    InflateRect(rgrc[0], -Integer(FBorderSize), -Integer(FBorderSize));
  end;
end;

procedure TNxControl6.WMNCPaint(var Message: TMessage);
var
  Device: HDC;
  Pen: HPEN;
  i: Integer;
  R: TRect;
  P: array[0..2] of TPoint;
  HightlightColor, ShadowColor: TColor;
begin
  { Required for ScrollBars }
  inherited;

  { Can draw? }
  if BorderSize > 0 then
  begin
    case BorderStyle of
      btSolid:
      begin
        HightlightColor := FBorderColor;
        ShadowColor := FBorderColor;
      end;
      btLowered:
      begin
        HightlightColor := clBtnHighlight;
        ShadowColor := clBtnShadow;
      end;
      else
      begin
        HightlightColor := clBtnShadow;
        ShadowColor := clBtnHighlight;
      end;
    end;

    Device := GetWindowDC(Handle);
    try
      GetWindowRect(Handle, R);
      OffsetRect(R, -R.Left, -R.Top);

      Pen := CreatePen(PS_SOLID, 1, ColorToRGB(ShadowColor));
      try
        SelectObject(Device, Pen);

        P[0] := Point(R.Left, R.Bottom - 1);
        P[1] := R.TopLeft;
        P[2] := Point(R.Right, R.Top);

        Polyline(Device, P, 3);

        for i := 2 to BorderSize do
        begin
          Inc(P[0].X);
          Dec(P[0].Y);
          Inc(P[1].X);
          Inc(P[1].Y);
          Dec(P[2].X);
          Inc(P[2].Y);

          Polyline(Device, P, 3);
        end;

      finally
        DeleteObject(Pen);
      end;

      Pen := CreatePen(PS_SOLID, 1, ColorToRGB(HightlightColor));
      try
        SelectObject(Device, Pen);

        P[0] := Point(R.Left + 1, R.Bottom - 1);
        P[1] := Point(R.Right - 1, R.Bottom - 1);
        P[2] := Point(R.Right - 1, R.Top);

        Polyline(Device, P, 3);

        for i := 2 to BorderSize do
        begin
          Inc(P[0].X);
          Dec(P[0].Y);
          Dec(P[1].X);
          Dec(P[1].Y);
          Dec(P[2].X);
          Inc(P[2].Y);

          Polyline(Device, P, 3);
        end;

      finally
        DeleteObject(Pen);
      end;

    finally
      ReleaseDC(Handle, Device);
    end;

  end;

end;

{ TNxPersistentScrollBar6 }

constructor TNxPersistentScrollBar6.Create;
begin
  FLargeChange := 50;
  FMax := 100;
  FPosition := 0;
  FSmallChange := 1;
  FVisible := True;
end;

procedure TNxPersistentScrollBar6.DoChange;
begin
  if Assigned(FOnChange) then FOnChange(Self);
end;

function TNxPersistentScrollBar6.GetLargeChange: Integer;
begin
  Result := FLargeChange;
end;

function TNxPersistentScrollBar6.GetMax: Integer;
begin
  Result := FMax;
end;

function TNxPersistentScrollBar6.GetPageSize: Integer;
begin
  Result := FPageSize;
end;

function TNxPersistentScrollBar6.GetPosition: Integer;
begin
  Result := FPosition;
end;

function TNxPersistentScrollBar6.GetSmallChange: Integer;
begin
  Result := FSmallChange;
end;

function TNxPersistentScrollBar6.GetVisible: Boolean;
begin
  Result := FVisible;
end;

procedure TNxPersistentScrollBar6.Next;
begin
  Position := Position + SmallChange;
end;

procedure TNxPersistentScrollBar6.PageDown;
begin
  Position := Position + LargeChange;
end;

procedure TNxPersistentScrollBar6.PageUp;
begin
  Position := Position - LargeChange;
end;

procedure TNxPersistentScrollBar6.Prior;
begin
  Position := Position - SmallChange;
end;

procedure TNxPersistentScrollBar6.SetLargeChange(const Value: Integer);
begin
  FLargeChange := Value;
end;

procedure TNxPersistentScrollBar6.SetMax(const Value: Integer);
begin
  FMax := Value;
end;

procedure TNxPersistentScrollBar6.SetPageSize(const Value: Integer);
begin
  FPageSize := Value;
end;

procedure TNxPersistentScrollBar6.SetPosition(const Value: Integer);
var
  FOldPosition: Integer;
begin
  FOldPosition := FPosition;
  FPosition := Value;
  if FPosition < 0 then FPosition := 0;
  if FPosition + FPageSize > FMax then FPosition := (FMax - FPageSize) + 1;
  if FPosition <> FOldPosition then DoChange;
end;

procedure TNxPersistentScrollBar6.SetSmallChange(const Value: Integer);
begin
  FSmallChange := Value;
end;

procedure TNxPersistentScrollBar6.SetVisible(const Value: Boolean);
begin
  FVisible := Value;
end;

{ TNxScrollControl6 }

constructor TNxScrollControl6.Create(AOwner: TComponent);
begin
  inherited;
  FHorzScrollBar := TNxScrollBar.Create(Self, sbHorizontal);
  FScrollBars := [sbHorizontal, sbVertical];
  FVertScrollBar := TNxScrollBar.Create(Self, sbVertical);

  { Note: BorderWidth cause bug
          in ScrollBar re-paint, so
          it need to be 0! }
//  BorderWidth := 0;
end;

procedure TNxScrollControl6.CreateParams(var Params: TCreateParams);
begin
  inherited;
  with Params do Style := Style or WS_HSCROLL or WS_VSCROLL or WS_CLIPCHILDREN;
end;

procedure TNxScrollControl6.CreateWnd;
begin
  inherited;

  SetBorderStyle(FBorderStyle);

  { This is the place where scroll-bars
    need to be initialized }
  SetScrollBars(FScrollBars);
end;

destructor TNxScrollControl6.Destroy;
begin
  { Destroy Obj. }
  FHorzScrollBar.Free;
  FVertScrollBar.Free;

  inherited Destroy;
end;

procedure TNxScrollControl6.DoHorizontalScroll;
begin
  if Assigned(FOnHorizontalScroll) then FOnHorizontalScroll(Self);  
end;

function TNxScrollControl6.DoMouseWheelDown(Shift: TShiftState;
  MousePos: TPoint): Boolean;
begin
  Result := True;
end;

function TNxScrollControl6.DoMouseWheelUp(Shift: TShiftState;
  MousePos: TPoint): Boolean;
begin
  Result := True;
end;

procedure TNxScrollControl6.DoVerticalScroll;
begin
  if Assigned(FOnVerticalScroll) then FOnVerticalScroll(Self);  
end;

function TNxScrollControl6.GetHandle: HWND;
begin
  Result := inherited Handle;
end;

function TNxScrollControl6.GetHorzScrollBar: TNxScrollBar;
begin
  Result := FHorzScrollBar;
end;

function TNxScrollControl6.GetScrollBars: TScrollStyle;
begin
  Result := ssNone;
end;

function TNxScrollControl6.GetScrollType(ScrollCode: Integer): TNxScrollType;
begin
  case ScrollCode of
    SB_LINEDOWN: Result := stSmallIncrement;
    SB_LINEUP: Result := stSmallDecrement;
    SB_PAGEDOWN: Result := stLargeIncrement;
    SB_PAGEUP: Result := stLargeDecrement;
    SB_THUMBPOSITION: Result := stThumbPosition;
    SB_THUMBTRACK: Result := stThumbTrack;
    SB_TOP: Result := stFirst;
    SB_BOTTOM: Result := stLast;
    else Result := stEndScroll;
  end;
end;

function TNxScrollControl6.GetVertScrollBar: TNxScrollBar;
begin
  Result := FVertScrollBar;
end;

function TNxScrollControl6.IsDestroying: Boolean;
begin
  Result := csDestroying in ComponentState;
end;

function TNxScrollControl6.IsReading: Boolean;
begin
  Result := csReading in ComponentState;
end;

procedure TNxScrollControl6.ScrollContentBy(DeltaX, DeltaY: Integer);
begin
  if Assigned(FOnContentScroll)
    then FOnContentScroll(Self);
end;

procedure TNxScrollControl6.ScrollRect(DeltaX, DeltaY: Integer; Rect,
  ClipRect: TRect);
begin

end;

procedure TNxScrollControl6.SelectNextControl;
begin
  if (Parent is TWinControl) then
    PostMessage((Parent as TWinControl).Handle, WM_KEYDOWN, VK_TAB, 0);
end;

procedure TNxScrollControl6.SelectPrevControl;
begin
  if (Parent is TWinControl) then
  begin
    PostMessage((Parent as TWinControl).Handle, WM_KEYDOWN, VK_SHIFT, 0);
    PostMessage((Parent as TWinControl).Handle, WM_KEYDOWN, VK_TAB, 0);
  end;
end;

procedure TNxScrollControl6.SetScrollBars(const Value: TNxScrollBars);
begin
  FScrollBars := Value;
  if HandleAllocated then
  begin
    FHorzScrollBar.Visible := sbHorizontal in FScrollBars;
    FVertScrollBar.Visible := sbVertical in FScrollBars;
    Invalidate;
  end;
end;

procedure TNxScrollControl6.WMHScroll(var Message: TWMHScroll);
var
  ScrollType: TNxScrollType;
begin
  ScrollType := GetScrollType(Message.ScrollCode);

  HorzScrollBar.Scroll(ScrollType);

  { Event }
  if ScrollType <> stEndScroll then DoHorizontalScroll;
end;

procedure TNxScrollControl6.WMVScroll(var Message: TWMVScroll);
var
  ScrollType: TNxScrollType;
begin
	inherited;
  { Note: After first occur, Msg occur once
          more to send SB_ENDSCROLL ScrollCode }

  ScrollType := GetScrollType(Message.ScrollCode);

  VertScrollBar.Scroll(ScrollType);

  { Event }
  if ScrollType <> stEndScroll then DoVerticalScroll;
end;

{ TNxCustomScrollBar }

procedure TNxCustomScrollBar.Assign(Source: TPersistent);
begin
  inherited;

end;

procedure TNxCustomScrollBar.CheckValues;
begin
  { Note: Max is zero-based }
//  FMax := Math.Max(0, FMax);

  { Note: Max set to 0 will hide Bar. Hide
          Bar if PageSize > Max }
  if (FPageSize > FMax)
    or (FPageSize = 0)
    or (FMax = 0) then
  begin
    FPosition := 0;
  end;
end;

procedure TNxCustomScrollBar.Clear(Update: Boolean);
begin
  FMax := 0;
  FPageSize := 0;
  FPosition := 0;

  { Upon request }
  if Update then Self.Update;
end;

constructor TNxCustomScrollBar.Create(AControl: INxScrollingControl;
  AKind: TScrollBarKind);
begin
  FAutoHide := True;
  FControl := AControl;
  FEnabled := True;
  FKind := AKind;
  FLargeChange := 10;
  FLocked := False;
  FMax := 0;
  FMin := 0;
  FPosition := 0;
  FSmallChange := 1;
  FSnapshotPosition := -1;
  FUpdating := False;
  FVisible := True;
end;

destructor TNxCustomScrollBar.Destroy;
begin

  inherited;
end;

procedure TNxCustomScrollBar.EraseSnapshot;
begin
  FSnapshotPosition := -1;
end;

procedure TNxCustomScrollBar.First;
begin
	if Max > 0 then
		case Kind of
  	  sbHorizontal: SendMessage(FControl.Handle, WM_HSCROLL, SB_TOP, 0);
	    sbVertical: SendMessage(FControl.Handle, WM_VSCROLL, SB_TOP, 0);
	  end;
end;

function TNxCustomScrollBar.GetControlScrollBars: TScrollStyle;
var
  Flags: DWORD;
begin
  Flags := GetWindowLong(FControl.Handle, GWL_STYLE) and (WS_VSCROLL or WS_HSCROLL);
  case Flags of
    0: Result := ssNone;
    WS_VSCROLL: Result := ssVertical;
    WS_HSCROLL: Result := ssHorizontal;
    else Result := ssBoth;
  end;
end;

function TNxCustomScrollBar.GetFlag: Integer;
begin
  case Kind of
    sbHorizontal: Result := SB_HORZ;
    else Result := SB_VERT;
  end;
end;

function TNxCustomScrollBar.GetInfoFlag: Cardinal;
begin
  case FScrollKind of
    sbHorizontal: Result := OBJID_HSCROLL;
    else Result := OBJID_VSCROLL;
  end;
end;

function TNxCustomScrollBar.GetPosition: Integer;
begin
  if Showing
    and not FLocked then
  begin
    Result := ScrollInfo.nPos;
  end
  else Result := FPosition;
end;

function TNxCustomScrollBar.GetScrollInfo: TScrollInfo;
begin
  Result.cbSize := SizeOf(TScrollInfo);
  Result.fMask := SIF_ALL;

  { Win32 Call }
  Windows.GetScrollInfo(FControl.Handle, Flag, Result);
end;

function TNxCustomScrollBar.GetShowing: Boolean;
begin
  Result := GetControlScrollBars = ssBoth;
  case Kind of
    sbHorizontal: Result := Result or (GetControlScrollBars = ssHorizontal);
    sbVertical: Result := Result or (GetControlScrollBars = ssVertical);
  end;
end;

function TNxCustomScrollBar.GetSnapshotPosition: Integer;
begin
  Result := FSnapshotPosition;
  if Result = -1 then Result := Position;
end;

function TNxCustomScrollBar.GetThumbPosition: Integer;
var
  Info: TScrollInfo;
begin
  Info.cbSize := SizeOf(TScrollInfo);
  Info.fMask := SIF_TRACKPOS;

  { Win32 Call }
  Windows.GetScrollInfo(FControl.Handle, Flag, Info);
  Result := Info.nTrackPos;
end;

procedure TNxCustomScrollBar.Hide;
begin
  Windows.ShowScrollBar(FControl.Handle, Flag, False);
end;

function TNxCustomScrollBar.IsFirst: Boolean;
begin
  Result := ScrollInfo.nPos = 0;
end;

function TNxCustomScrollBar.IsLast: Boolean;
begin
  Result := ScrollInfo.nPos > ScrollInfo.nMax - Integer(ScrollInfo.nPage);
end;

function TNxCustomScrollBar.IsUpdating: Boolean;
begin
  Result := FUpdating;
end;

procedure TNxCustomScrollBar.Last;
begin
	if Max > 0 then
		case Kind of
  	  sbHorizontal: SendMessage(FControl.Handle, WM_HSCROLL, SB_BOTTOM, 0);
	    sbVertical: SendMessage(FControl.Handle, WM_VSCROLL, SB_BOTTOM, 0);
	  end;
end;

procedure TNxCustomScrollBar.Lock;
begin
  FLocked := True;
end;

procedure TNxCustomScrollBar.MoveBy(Distance: Integer);
begin
  Position := Position + Distance;
end;

procedure TNxCustomScrollBar.Next;
begin
	if Max > 0 then
		case Kind of
  	  sbHorizontal: SendMessage(FControl.Handle, WM_HSCROLL, SB_LINERIGHT, 0);
	    sbVertical: SendMessage(FControl.Handle, WM_VSCROLL, SB_LINEDOWN, 0);
	  end;
end;

procedure TNxCustomScrollBar.PageDown;
begin
	if Max > 0 then
		case Kind of
	    sbHorizontal: SendMessage(FControl.Handle, WM_HSCROLL, SB_PAGERIGHT, 0);
	    sbVertical: SendMessage(FControl.Handle, WM_VSCROLL, SB_PAGEDOWN, 0);
	  end;
end;

procedure TNxCustomScrollBar.PageUp;
begin
	if Max > 0 then
		case Kind of
	    sbHorizontal: SendMessage(FControl.Handle, WM_HSCROLL, SB_PAGELEFT, 0);
	    sbVertical: SendMessage(FControl.Handle, WM_VSCROLL, SB_PAGEUP, 0);
	  end;
end;

procedure TNxCustomScrollBar.Prior;
begin
	if Max > 0 then
		case Kind of
	    sbHorizontal: SendMessage(FControl.Handle, WM_HSCROLL, SB_LINELEFT, 0);
	    sbVertical: SendMessage(FControl.Handle, WM_VSCROLL, SB_LINEUP, 0);
	  end;
end;

procedure TNxCustomScrollBar.Scroll(ScrollType: TNxScrollType);
var
  ScrollPos: Integer;
begin
  case ScrollType of
    stFirst: ScrollPos := 0;
    stLast: ScrollPos := Max;
    stSmallDecrement: ScrollPos := Position - SmallChange;
    stSmallIncrement: ScrollPos := Position + SmallChange;
    stLargeDecrement: ScrollPos := Position - LargeChange;
    stLargeIncrement: ScrollPos := Position + LargeChange;
    stThumbPosition: ScrollPos := ThumbPosition;
    stThumbTrack: ScrollPos := ThumbPosition;
    else Exit;
  end;

  Position := ScrollPos;
end;

procedure TNxCustomScrollBar.SetAutoHide(const Value: Boolean);
begin
  if Value <> FAutoHide then
  begin
    FAutoHide := Value;
    Update;
  end;
end;

procedure TNxCustomScrollBar.SetEnabled(const Value: Boolean);
begin

end;

procedure TNxCustomScrollBar.SetKind(const Value: TScrollBarKind);
begin

end;

procedure TNxCustomScrollBar.SetManualScroll(const Value: Boolean);
begin

end;

procedure TNxCustomScrollBar.SetMax(const Value: Integer);
begin
  FMax := Value;

  Update; { Set ScrollBar by Win32 }
end;

procedure TNxCustomScrollBar.SetMin(const Value: Integer);
begin
  FMin := Value;

  Update; { Set ScrollBar by Win32 }
end;

procedure TNxCustomScrollBar.SetPageSize(const Value: Integer);
begin
  FPageSize := Value;

  { Note: Max set to 0 will hide Bar. Hide
          Bar if PageSize > Max }
  if FPageSize > FMax
    then Clear;

  Update; { Set ScrollBar by Win32 }
end;

procedure TNxCustomScrollBar.SetPosition(const Value: Integer);
var
  FPrevPosition: Integer;
begin
  FPrevPosition := 0;

  { Remember Pos. before Lock }
  if not FLocked
    then FPrevPosition := Position;

  { Note: FPosition is set even if Lock }
  FPosition := Value;

  { Check bounds }
  if FPosition + PageSize > Max then FPosition := (Max - PageSize) + 1;
  if FPosition < 0 then FPosition := 0;

  { Set ScrollBar by
    Win32 (SetScrollInfo) }
  Update;

  if not FLocked then
    case Kind of
      sbHorizontal: FControl.ScrollContentBy(Position - FPrevPosition, 0);
      sbVertical: FControl.ScrollContentBy(0, Position - FPrevPosition);
    end;
end;

procedure TNxCustomScrollBar.SetValues(AMax, APageSize: Integer);
begin
  FMax := AMax;
  FPageSize := APageSize;

  CheckValues;

  { Set ScrollBar by Win32 }
  Update;
end;

procedure TNxCustomScrollBar.SetVisible(const Value: Boolean);
begin
  FVisible := Value;
  if Showing <> Visible then
  begin
    ShowScrollBar(FControl.Handle, Flag, FVisible);
  end;
end;

function TNxCustomScrollBar.ShouldBeVisible: Boolean;
begin
  Result := (FMax > FPageSize) or not AutoHide;
end;

procedure TNxCustomScrollBar.Show;
begin
  Windows.ShowScrollBar(FControl.Handle, Flag, True);
end;

procedure TNxCustomScrollBar.Snapshot;
begin
  FSnapshotPosition := Position;
end;

procedure TNxCustomScrollBar.Unlock(Update: Boolean);
begin
  if FLocked then
  begin
    FLocked := False;

    if Update then Self.Update; { Set ScrollBar by Win32 }

    { ToDo: Scroll Content }
  end;
end;

procedure TNxCustomScrollBar.Update;
const
  EnableBar: array[Boolean] of DWORD = (ESB_DISABLE_BOTH, ESB_ENABLE_BOTH);
var
  ScrollInfo: TScrollInfo;
  Flags, nMax, nPage: Integer;
  NeedSet, Unchanged: Boolean;
begin
  if FUpdating or FLocked then Exit;

  { Required }
  if Assigned(FControl)
    and FControl.HandleAllocated
    and not FControl.IsDestroying
  then
  begin

    try
      FUpdating := True;

      Flags := SIF_ALL;

      if not AutoHide then Flags := Flags or SIF_DISABLENOSCROLL;

      nMax := FMax;
      nPage := FPageSize;

      { Hide or disable Bar }

      { Count is 0 }
      if nMax = -1 then nMax := 0;

      case Kind of
        sbHorizontal:
          if nPage > nMax then
          begin
            nMax := 0;
          end;
      end;

      ScrollInfo.nMin := FMin;
      ScrollInfo.nPage := nPage;
      ScrollInfo.nMax := nMax;
      ScrollInfo.nPos := FPosition;
      ScrollInfo.nTrackPos := FPosition;
      ScrollInfo.fMask := Flags;
      ScrollInfo.cbSize := SizeOf(ScrollInfo);

      NeedSet := True;

      if not AutoHide then
      begin
        Unchanged := (nMax = 0) and (GetScrollInfo.nMax = 0);

        NeedSet := not Unchanged;
      end;

      { NOTICE: SetScrollInfo need to be called! }

      { Set ScrollBar Prop. in Win }
      if Visible then
      begin

        { Buttons bug. }
        if NeedSet then
        begin
          SetScrollInfo(FControl.Handle, Flag, ScrollInfo, True);
        end;

        if FPageSize < FMax then
        begin
          { 6/20/07:  Disable scrollbar when control is not enabled. If scrollbar is already
                      disabled, it will not be enabled. }
          EnableScrollBar(FControl.Handle, Flag, EnableBar[FControl.Enabled and FEnabled and (FMax > 0)]);
        end;
      end;

    finally
      FUpdating := False;
    end;

  end;

end;

procedure TNxCustomScrollBar.UpdateScrollBar;
begin

end;

{ TNxFlyoverControl6 }

procedure TNxFlyoverControl6.CreateParams(var Params: TCreateParams);
begin
  inherited;
  with Params do
  begin
    WndParent := GetDesktopWindow;
    Style := WS_CLIPSIBLINGS or WS_CHILD or WS_TABSTOP;
    ExStyle := WS_EX_TOPMOST or WS_EX_TOOLWINDOW;
    WindowClass.Style := CS_DBLCLKS or CS_SAVEBITS;
  end;
end;

{ TNxTextFitHintWindow }

constructor TNxTextFitHintWindow.Create(AOwner: TComponent);
begin
  inherited;
  Color := clWindow;
end;

procedure TNxTextFitHintWindow.CreateParams(var Params: TCreateParams);
begin
  inherited;
  with Params do
  begin
    WindowClass.Style := WindowClass.Style and not CS_DROPSHADOW;
  end;
end;

{ TNxScrollBarView }

constructor TNxScrollBarView.Create(AOwner: IViewOwner);
begin
  FOwner := AOwner;
end;

end.

