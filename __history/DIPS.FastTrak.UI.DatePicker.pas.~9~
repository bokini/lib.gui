{ ------------------------------------------------------------- }
{ Purpose : DIPS FastTrak UI                                    }
{ By      : Bojan Nikolic                                       }
{ For     : DIPS AS                                             }
{ ------------------------------------------------------------- }
{ Copyright (C) DIPS AS 2020. All Rights Reserved.              }
{ ------------------------------------------------------------- }
unit DIPS.FastTrak.UI.DatePicker;

interface

uses
  System.Classes, System.Types,
  Vcl.Graphics,
  DIPS.FastTrak.UI.Intf,
  DIPS.FastTrak.UI.Types,
  DIPS.FastTrak.UI.Edit,
  DIPS.FastTrak.UI.Popup;

type
  { TFtDatePicker }

  TFtDatePicker = class( TFtDropDownEdit, IDatePickerOwner )
  private
    FOnCustomDrawDateColor: TNxCustomDrawDate;
    FOnGetDateColor: TNxGetDateColor;
    FSelectionDateRange: TDateRange;
    FStartDayOfWeek: TStartDayOfWeek;
    { Property Accessors }
    function GetDate: TDate;
    procedure SetDate( const Value: TDate );
    procedure SetStartDayOfWeek( const Value: TStartDayOfWeek );
  protected
    { Event Handlers }
    procedure DoCustomDrawDate( Sender: TObject; Date: TDateTime; DayRect: TRect ); dynamic;
    procedure DoGetDateColor( Sender: TObject; Date: TDateTime; var DayColor: TColor ); dynamic;
    { Keyboard }
    procedure KeyDown( var Key: Word; Shift: TShiftState ); override;
    { Virtual Methods }
    function CreatePopupControl: IPopupControl; override;
  public
    constructor Create( AOwner: TComponent ); override;
  published
    property Date: TDate read GetDate write SetDate;
    property SelectionDateRange: TDateRange read FSelectionDateRange write FSelectionDateRange default drAll;
    property StartDayOfWeek: TStartDayOfWeek read FStartDayOfWeek write SetStartDayOfWeek default dwSystem;
    property OnCustomDrawDate: TNxCustomDrawDate read FOnCustomDrawDateColor write FOnCustomDrawDateColor;
    property OnGetDateColor: TNxGetDateColor read FOnGetDateColor write FOnGetDateColor;
  end;

implementation


{ TFtDatePicker }

constructor TFtDatePicker.Create( AOwner: TComponent );
begin
  inherited;
  FSelectionDateRange := drAll;
  FStartDayOfWeek := dwSystem;
end;

function TFtDatePicker.CreatePopupControl: IPopupControl;
var
  DateControl: IDateControl;
begin
  Result := TDatePopupControl.Create( Self );
  if Supports( Result, IDateControl, DateControl ) then
  begin
    DateControl.SelectionDateRange := SelectionDateRange;
    DateControl.StartDayOfWeek := StartDayOfWeek;
  end;
end;

procedure TFtDatePicker.DoCustomDrawDate( Sender: TObject; Date: TDateTime; DayRect: TRect );
begin
  if Assigned( FOnCustomDrawDateColor ) then
    FOnCustomDrawDateColor( Self, Date, DayRect );
end;

procedure TFtDatePicker.DoGetDateColor( Sender: TObject; Date: TDateTime; var DayColor: TColor );
begin
  if Assigned( FOnGetDateColor ) then
    FOnGetDateColor( Self, Date, DayColor );
end;

function TFtDatePicker.GetDate: TDate;
begin
  Result := StrToDateDef( Text, Today );
end;

procedure TFtDatePicker.KeyDown( var Key: Word; Shift: TShiftState );
begin
  inherited;
  if DroppedDown then
    PopupControl.KeyDown( Key, Shift );
end;

procedure TFtDatePicker.SetDate( const Value: TDate );
begin
  Text := DateToStr( Value );
end;

procedure TFtDatePicker.SetStartDayOfWeek( const Value: TStartDayOfWeek );
begin
  FStartDayOfWeek := Value;
end;

end.
