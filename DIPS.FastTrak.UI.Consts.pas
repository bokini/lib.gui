unit DIPS.FastTrak.UI.Consts;

interface

uses
  Vcl.Graphics,
  {Winapi}
  Winapi.Windows;

const
{$REGION 'DIPS Colors Constants'}
  clListBoxAlternationBackground = $00F6F6F6;
  clBtnFaceDefault               = $009D6A00;
  clBtnFaceDefaultHot            = $009D6A00;
  clBtnFaceDefaultPressed        = $00654400;
  clBtnFaceDropDown              = $00E0E0E0;
  clBtnFaceNormal                = $00D5D5D5;
  clBtnFaceNormalHot             = $00BEBEBE;
  clBtnFaceNormalPressed         = $00898989;
  clContainerBackground          = $00EEEEEE; // NormalContainer_Background_WhenFormStyleInfieldTopAlignedLabels
  clDatePickerDayHot             = $00F7F6F3;
  clDatePickerToday              = $00F1EFE8;
  clDatePickerTodayBorder        = $00E7E5DD;
  clEditControl                  = $00FEFEFE;
  clEditControlBorder            = $00A3A3A3;
  clErrorGeometryFill            = $005E5EC9;
  clGlyph                        = $00444444;
  clHyperlinkForeground          = $009E6F08;
  clInfoGeometryFill             = $00926E1E;
  clLabelForeground              = $00444444;
  clMandatoryBar                 = $001685F6;
  clPopupBorder                  = $00080808;
  clReadOnlyDashedLine           = $00BCBCBC;
  clReadOnlySolidLine            = clBtnFaceNormal;
  clSelectedBackground           = $00E9D9C8;
  clSelectedBackgroundDark       = $00897F04;
  clSelectedBackgroundHot        = $00E8E6E0;
  clCheckBoxBorder               = $00A7A7A7;
  clDotWideSpaceLine             = $00C3C3C3;
  clDotWideSpaceLineBackground   = $00DEDEDE;
{$ENDREGION}
{$REGION 'Dynamic Colors}
  ButtonColor: array [boolean] of integer        = ( clBtnFaceNormal, clBtnFaceDefault );
  ButtonColorHot: array [boolean] of integer     = ( clBtnFaceNormalHot, clBtnFaceDefaultHot );
  ButtonColorPressed: array [boolean] of integer = ( clBtnFaceNormalPressed, clBtnFaceDefaultPressed );
  ButtonColorText: array [boolean] of integer    = ( clBlack, clWhite );
{$ENDREGION}

const
  { Misc }
  CONTROL_FRAME       = $00A5A2A2;
  SELECTED_TEXT_COLOR = $00FFFFFF;

{$REGION 'Radio button'}
  RadioButtonRadius   = 15;
  RadioButtonsSpacing = 16;
  RADIO_FRAME_COLOR   = CONTROL_FRAME;
  RADIO_CHECK_COLOR   = $00AE8303;
{$ENDREGION}
{$REGION 'Button(s)'}
  DropDownButtonWidth = 16;
  ExecuteButtonWidth  = 21;
{$ENDREGION}
{$REGION 'Edit'}
  EditHeight            = 20; // TEdit.Height
  EditPlaceholderHeight = 20; // Placeholder TRect.Height when not editing
  EditWidth             = 84;
  PaddingEdit           = 2; // Border to content padding inside TEdit
  PreviewWidth          = 20;
{$ENDREGION}
{$REGION 'ComboBox'}
  DropDownCountDefault = 8;
{$ENDREGION}
{$REGION 'Validation'}
  ValidColor: array [boolean] of integer = ( $005C5CCD, clBlue );
{$ENDREGION}
{$REGION 'Input control'}
  TagColorBlending   = 200;
  MandatoryBarWidth  = 3;
  PageHeaderFontSize = 12;
{$ENDREGION}
{$REGION 'Winapi'}
  ControlVisible: array [boolean] of integer = ( SW_HIDE, SW_SHOW );
{$ENDREGION}
{$REGION 'Unsorted'}
  CheckBoxIndent     = 4;
  ComboBoxItemHeight = 24;
  szItemHeight       = 13;
{$ENDREGION}

resourcestring
  StrToday = 'i dag';
  StrReset = 'Nullstill';
  StrUnanswered = '(Ubesvart)';

var
  { Padding & Spacing }
  PaddingCell: integer = 5;

  SpacingThin: integer = 2;
  SpacingDouble: integer = 4;
  SpacingQuadrupled: integer = 8;

implementation

end.
