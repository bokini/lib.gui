{ ------------------------------------------------------------- }
{ Purpose : DIPS FastTrak UI }
{ By      : Bojan Nikolic }
{ For     : DIPS AS }
{ ------------------------------------------------------------- }
{ Copyright (C) DIPS AS 2020. All Rights Reserved. }
{ ------------------------------------------------------------- }
unit DIPS.FastTrak.UI.Popup;

interface

uses
  {Standard}
  Classes, UITypes, System.Types,
  {System}
  ExtCtrls,
  {Controls}
  Controls, StdCtrls, Forms, Dialogs,
  {Winapi}
  Winapi.Windows, Winapi.Messages,
  {Vcl}
  Vcl.Graphics,
  {DIPS}
  DIPS.FastTrak.UI.Intf,
  DIPS.FastTrak.UI.Classes,
  DIPS.FastTrak.UI.Consts,
  DIPS.FastTrak.UI.Types,
  DIPS.FastTrak.UI.Buttons,
  DIPS.FastTrak.UI.Controls;

type
  { TPopupControl }

  TPopupControl = class( TNxScrollControl6, IPopupControl )
  private
    FDropDown: IDropDown;
    FMouseDown: Boolean;
    FPopupState: TPopupState;
    FShowShadow: Boolean;
    function GetControlRect: TRect;
    function GetDropDown: IDropDown;
    procedure SetDropDown( const Value: IDropDown );
    procedure SetShowShadow( const Value: Boolean );
  protected
    function GetCanvas: TCanvas;
    function GetFont: TFont;
    procedure AfterPopup; virtual;
    procedure BeforePopup; virtual;
    { Delphi Messages }
    procedure CMMouseEnter( var Message: TMessage ); message CM_MOUSEENTER;
    procedure CMMouseLeave( var Message: TMessage ); message CM_MOUSELEAVE;
    { Common Virtual Methods }
    procedure CreateParams( var Params: TCreateParams ); override;
    procedure CreateWnd; override;
    procedure UpdatePopup; virtual;
    procedure WndProc( var Message: TMessage ); override;
    { Mouse & Keyboard }
    procedure MouseDown( Button: TMouseButton; Shift: TShiftState; X, Y: Integer ); override;
    procedure MouseMove( Shift: TShiftState; X, Y: Integer ); override;
    procedure MouseUp( Button: TMouseButton; Shift: TShiftState; X, Y: Integer ); override;
    { Windows Messages }
    procedure WMNCMouseLeave( var Message: TMessage ); message WM_NCMOUSELEAVE;
    procedure WMNCPaint( var Message: TMessage ); message WM_NCPAINT;
    { Properties }
    property PopupState: TPopupState read FPopupState;
  public
    constructor Create( AOwner: TComponent ); override;
    { Methods }
    procedure Close( Value: WideString ); virtual;
    procedure Popup( const X, Y: Integer; Anchor: TPopupAnchor = paLeft ); overload; virtual;
    procedure Popup( const Location: TPoint; Anchor: TPopupAnchor = paLeft ); overload; virtual;
    { Properties }
    property ControlRect: TRect read GetControlRect;
    property DropDown: IDropDown read GetDropDown write SetDropDown;
    property Font;
    property Height;
    property ShowShadow: Boolean read FShowShadow write SetShowShadow;
    property Visible;
    property Width;
  end;

  { TEditPopupControl }

  TEditPopupControl = class( TPopupControl )
  private
    FEdit: TCustomEdit;
  public
    constructor Create( AOwner: TComponent ); override;
    procedure Close( Value: WideString ); override;
    { Properties }
    property Edit: TCustomEdit read FEdit;
  end;

  { TNxListPopupControl6 }

  INxListPopup = interface
    ['{4EDB0201-082C-4C68-BCEA-6339AE4B80DA}']
    procedure ScrollDown;
    procedure ScrollUp;
  end;

  TListPopupControl = class( TEditPopupControl, INxListPopup )
  private
    function GetPageItemCount: Integer;
  protected
    { Virtual Methods }
    function GetCount: Integer; virtual; abstract;
    function GetDropDownCount: Integer; virtual; abstract;
    function GetItemIndex: Integer; virtual; abstract;
    procedure SetItemIndex( const Value: Integer ); virtual; abstract;
    procedure KeyDown( var Key: Word; Shift: TShiftState ); override;
    procedure Paint; override;
    function PaintCell( const Index: Integer; CellRect: TRect; State: TOwnerDrawState ): Boolean; virtual;
    procedure ScrollDown;
    procedure ScrollToItem( const Index: Integer );
    procedure ScrollUp;
    procedure UpdateVertScrollBar; virtual;
  public
    property Count: Integer read GetCount;
    property DropDownCount: Integer read GetDropDownCount;
    property ItemIndex: Integer read GetItemIndex write SetItemIndex;
    property PageItemCount: Integer read GetPageItemCount;
  end;

  { TStringPopupControl }

  TStringsPopupControl = class( TListPopupControl )
  private
    FHotIndex: Integer;
    function GetItemHeight: Integer;
    function GetItems: TStrings;
    function GetStringsControl: IStringsControl;
    procedure SetHotIndex( const Value: Integer );
  protected
    procedure AfterPopup; override;
    procedure BeforePopup; override;
    function GetCount: Integer; override;
    function GetDropDownCount: Integer; override;
    function GetItemIndex: Integer; override;
    procedure SetItemIndex( const Value: Integer ); override;
    procedure UpdatePopup; override;
    { Mouse & Keyboard Methods }
    procedure MouseMove( Shift: TShiftState; X, Y: Integer ); override;
    procedure MouseUp( Button: TMouseButton; Shift: TShiftState; X, Y: Integer ); override;
    { Paint Methods }
    procedure Paint; override;
    // function PaintCell(const Index: Integer; CellRect: TRect; Selected: Boolean): Boolean; override;
    procedure RefreshItem( const Index: Integer );
    procedure ScrollContentBy( DeltaX, DeltaY: Integer ); override;
    procedure ScrollRect( DeltaX, DeltaY: Integer; Rect, ClipRect: TRect ); override;
    procedure UpdateVertScrollBar; override;
    { Windows Messages }

    { Properties }
    property HotIndex: Integer read FHotIndex write SetHotIndex;
    property StringsControl: IStringsControl read GetStringsControl;
  public
    constructor Create( AOwner: TComponent ); override;
    function MeasureItemHeight( const Index: Integer; const AWidth: Integer ): Integer;
    { Properties }
    property ItemHeight: Integer read GetItemHeight;
    property Items: TStrings read GetItems;
  end;

  { TNxCheckPopupControl6 }

  TNxCheckPopupControl6 = class( TStringsPopupControl )
  private
    function GetCheckBoxSize: TSize;
  protected
    procedure MouseDown( Button: TMouseButton; Shift: TShiftState; X, Y: Integer ); override;
    procedure MouseUp( Button: TMouseButton; Shift: TShiftState; X, Y: Integer ); override;
    procedure RefreshCheckBox( const Index: Integer );
  public
    { Methods }
    function GetCheckBoxAtPos( X, Y: Integer ): Integer;
    function GetCheckBoxRect( const Index: Integer ): TRect;
    { Properties }
    property CheckBoxSize: TSize read GetCheckBoxSize;
  end;

  { TNxDatePickerView }

  TDatePopupControl = class;

  TDateSelectionMode = ( dmDays, dmMonths, dmYears );

  TNxCustomDrawDate = procedure( Sender: TObject; Date: TDateTime; DayRect: TRect ) of object;
  TNxGetDateColor = procedure( Sender: TObject; Date: TDateTime; var DayColor: TColor ) of object;

  TDatePickerView = class( TNxObjectView )
  private
    FColor: TColor;
    FDayHeight: Integer;
    FDayWidth: Integer;
    FDown: Boolean;
    FHotMonth: Integer;
    FHotDay: TDateTime;
    FMonth: Word;
    FOnGetDateColor: TNxGetDateColor;
    FPressedMonth: Integer;
    FSelected: TDateTime;
    FSelectionDateRange: TDateRange;
    FSelectionMode: TDateSelectionMode;
    FStartDay: TStartDayOfWeek;
    FValue: string;
    FYear: Word;
    FOnCustomDrawDate: TNxCustomDrawDate;
    { Property Accessors }
    function GetContentRect: TRect;
    function GetDatePopupControl: TDatePopupControl;
    function GetDayHeight: Integer;
    function GetDaysRect: TRect;
    function GetDayWidth: Integer;
    function GetHeaderTitleRect: TRect;
    function GetMonthSize: TSize;
    function GetMonthsRect: TRect;
    function GetWeekDaysRect: TRect;
    function GetWeeksRect: TRect;
    function GetYearSize: TSize;
    procedure SetColor( const Value: TColor );
    procedure SetDayHeight( const Value: Integer );
    procedure SetDayWidth( const Value: Integer );
    procedure SetDown( const Value: Boolean );
    procedure SetHotDay( const Value: TDateTime );
    procedure SetHotMonth( const Value: Integer );
    procedure SetMonth( const Value: Word );
    procedure SetSelected( const Value: TDateTime );
    procedure SetSelectionMode( const Value: TDateSelectionMode );
    procedure SetStartDay( const Value: TStartDayOfWeek );
    procedure SetYear( const Value: Word );
    procedure SetPressedMonth( const Value: Integer );
  protected
    function GetActualStartDay: TStartDayOfWeek;
    function GetDayInWeekText( DayInWeek: Integer ): WideString;
    function GetDayOfWeek( Day: TDateTime ): Integer;
    function GetHeaderRect: TRect;
    function GetLeftArrowRect: TRect;
    function GetMonthName( Month: Integer ): WideString;
    function GetRightArrowRect: TRect;
    { Event Handlers }
    procedure DoCustomDrawDate( Date: TDateTime; DayRect: TRect ); dynamic;
    procedure DoGetDateColor( Date: TDateTime; var DayColor: TColor ); dynamic;
    procedure PaintCell( const CellRect: TRect; const State: TButtonState; const Text: string ); virtual;
    procedure PaintDay( DayRect: TRect; Day: TDateTime ); virtual;
    procedure PaintDaysView( const AContentRect: TRect );
    procedure PaintHeader; virtual;
    procedure PaintMonthsView( const AContentRect: TRect );
    procedure PaintWeekDays( const X, Y: Integer ); virtual;
    procedure PaintYearsView( const AContentRect: TRect );
    { Properties }
    property DatePopupControl: TDatePopupControl read GetDatePopupControl;
  public
    constructor Create;
    { Painting Methods }
    function GetDayAtPos( const X, Y: Integer ): TDateTime;
    function GetDayRect( const Date: TDateTime ): TRect;
    function GetMonthAtPos( const X, Y: Integer ): Integer;
    function GetMonthRect( const AMonth: Integer ): TRect;
    function GetYearAtPos( const X, Y: Integer ): Integer;
    function IsShown( Date: TDateTime ): Boolean;
    class function IsValidDateSelection( const Date: TDateTime; const SelectionDateRange: TDateRange ): Boolean;
    procedure Changed;
    procedure FillDay( Date: TDateTime; DayRect: TRect; DayColor: TColor );
    procedure Paint; override;
    procedure PaintDays( const X, Y: Integer );
    procedure RefreshDay( const Date: TDateTime );
    procedure RefreshMonth( const AMonth: Integer );
    function TrySelect( const Date: TDateTime ): Boolean;
    { INxView }
    procedure MouseDown( Button: TMouseButton; Shift: TShiftState; X, Y: Integer ); override;
    procedure MouseLeave; override;
    procedure MouseMove( Shift: TShiftState; X, Y: Integer ); override;
    procedure MouseUp( Button: TMouseButton; Shift: TShiftState; X, Y: Integer ); override;
    { Properties }
    property Color: TColor read FColor write SetColor;
    property ContentRect: TRect read GetContentRect;
    property DayHeight: Integer read GetDayHeight write SetDayHeight;
    property DaysRect: TRect read GetDaysRect;
    property DayWidth: Integer read GetDayWidth write SetDayWidth;
    property Down: Boolean read FDown write SetDown;
    property HotDay: TDateTime read FHotDay write SetHotDay;
    property HotMonth: Integer read FHotMonth write SetHotMonth;
    property Month: Word read FMonth write SetMonth;
    property HeaderRect: TRect read GetHeaderRect;
    property HeaderTitleRect: TRect read GetHeaderTitleRect;
    property MonthsRect: TRect read GetMonthsRect;
    property MonthSize: TSize read GetMonthSize;
    property PressedMonth: Integer read FPressedMonth write SetPressedMonth;
    property Selected: TDateTime read FSelected write SetSelected;
    property SelectionDateRange: TDateRange read FSelectionDateRange write FSelectionDateRange;
    property SelectionMode: TDateSelectionMode read FSelectionMode write SetSelectionMode;
    property StartDay: TStartDayOfWeek read FStartDay write SetStartDay;
    property Value: string read FValue write FValue;
    property WeekDaysRect: TRect read GetWeekDaysRect;
    property WeeksRect: TRect read GetWeeksRect;
    property Year: Word read FYear write SetYear;
    property YearSize: TSize read GetYearSize;
    { Events }
    property OnCustomDrawDate: TNxCustomDrawDate read FOnCustomDrawDate write FOnCustomDrawDate;
    property OnGetDateColor: TNxGetDateColor read FOnGetDateColor write FOnGetDateColor;
  end;

  { TDatePopupControl }

  IDatePickerOwner = interface
    ['{C1358F28-0410-40DE-9848-28360D078580}']
    procedure DoCustomDrawDate( Sender: TObject; Date: TDateTime; DayRect: TRect );
    procedure DoGetDateColor( Sender: TObject; Date: TDateTime; var DayColor: TColor );
  end;

  IDateControl = interface
    ['{6EBFA4F7-B1AB-4B9E-A000-99DE8F3C1D9C}']
    procedure SetSelectionDateRange( const Value: TDateRange );
    procedure SetStartDayOfWeek( const Value: TStartDayOfWeek );
    property SelectionDateRange: TDateRange write SetSelectionDateRange;
    property StartDayOfWeek: TStartDayOfWeek write SetStartDayOfWeek;
  end;

  TDatePopupControl = class( TEditPopupControl, IPopupControl, IDateControl )
  private
    FNoneButton: TdcGraphicButton;
    FSelectionDateRange: TDateRange;
    FStartDayOfWeek: TStartDayOfWeek;
    FTodayButton: TdcGraphicButton;
    fView: TDatePickerView;
    function GetPickerOwner: IDatePickerOwner;
    procedure SetSelectionDateRange( const Value: TDateRange );
    procedure SetStartDayOfWeek( const Value: TStartDayOfWeek );
  protected
    procedure DoButtonClick( Sender: TObject );
    procedure DoGetDateColor( Sender: TObject; Date: TDateTime; var DayColor: TColor );
    procedure DoCustomDrawDate( Sender: TObject; Date: TDateTime; DayRect: TRect );
    function GetBestSize: TSize;
    { Protected Methods }
    procedure Paint; override;
    procedure KeyDown( var Key: Word; Shift: TShiftState ); override;
    procedure MouseDown( Button: TMouseButton; Shift: TShiftState; X, Y: Integer ); override;
    procedure MouseMove( Shift: TShiftState; X, Y: Integer ); override;
    procedure MouseUp( Button: TMouseButton; Shift: TShiftState; X, Y: Integer ); override;
  public
    constructor Create( AOwner: TComponent ); override;
    destructor Destroy; override;
    { Methods }
    procedure NextMonth;
    procedure PrevMonth;
    { Properties }
    property PickerOwner: IDatePickerOwner read GetPickerOwner;
    property StartDayOfWeek: TStartDayOfWeek read FStartDayOfWeek write SetStartDayOfWeek;
  end;

implementation

uses
  {Misc.}
  SysUtils, MultiMon, DateUtils, StrUtils, Math,
  DIPS.FastTrak.UI.Graphics,
  DIPS.FastTrak.UI.Helpers;

{ TNxPopupControl6 }

procedure TPopupControl.AfterPopup;
begin

end;

procedure TPopupControl.BeforePopup;
begin

end;

procedure TPopupControl.Close( Value: WideString );
begin
  if Assigned( DropDown ) and DropDown.AutoClose then
  begin
    { Set State }
    Include( FPopupState, psClosing );

    { Close & Destroy }
    DropDown.DroppedDown := False;
  end;
end;

procedure TPopupControl.CMMouseEnter( var Message: TMessage );
begin
  inherited;
  ReleaseCapture;
end;

procedure TPopupControl.CMMouseLeave( var Message: TMessage );
var
  CursorPos: TPoint;
begin
  inherited;

  GetCursorPos( CursorPos );
  if not PtInRect( BoundsRect, CursorPos ) then
  begin
    SetCaptureControl( Self );
  end;
end;

constructor TPopupControl.Create( AOwner: TComponent );
begin
  inherited;
  ControlStyle := ControlStyle + [csNoDesignVisible, csAcceptsControls];

  Supports( AOwner, IDropDown, FDropDown );

  FPopupState := [];

  { Init. Fields }
  FMouseDown := False;
  FShowShadow := True;

  { Init. Properties }
  BorderSize := 1;
  ScrollBars := [];
  Visible := False;
end;

procedure TPopupControl.CreateParams( var Params: TCreateParams );
const
  CS_DROPSHADOW = $00020000;
begin
  inherited CreateParams( Params );
  with Params do
  begin
    WndParent := GetDesktopWindow;
    Style := WS_CLIPSIBLINGS or WS_CHILD or WS_TABSTOP;
    ExStyle := WS_EX_TOPMOST or WS_EX_TOOLWINDOW;
    WindowClass.Style := CS_DBLCLKS or CS_SAVEBITS;
  end;

  { Enable drop shadow effect on
    Windows XP & later }
  if ShowShadow then

    if ( Win32Platform = VER_PLATFORM_WIN32_NT ) and ( ( Win32MajorVersion > 5 ) or ( ( Win32MajorVersion = 5 ) and ( Win32MinorVersion >= 1 ) ) ) then
      Params.WindowClass.Style := Params.WindowClass.Style or CS_DROPSHADOW;
end;

procedure TPopupControl.CreateWnd;
begin
  inherited;
  Winapi.Windows.SetParent( Handle, 0 );
end;

function TPopupControl.GetCanvas: TCanvas;
begin
  Result := Canvas;
end;

function TPopupControl.GetControlRect: TRect;
var
  TopLeft, BottomRight: TPoint;
begin
  TopLeft := ScreenToClient( BoundsRect.TopLeft );
  BottomRight := ScreenToClient( BoundsRect.BottomRight );
  Result := Rect( TopLeft.X, TopLeft.Y, BottomRight.X, BottomRight.Y );
end;

function TPopupControl.GetDropDown: IDropDown;
begin
  Result := FDropDown;
end;

function TPopupControl.GetFont: TFont;
begin
  Result := Font;
end;

procedure TPopupControl.MouseDown( Button: TMouseButton; Shift: TShiftState; X, Y: Integer );
begin
  inherited;

  FMouseDown := True; { Process Clicks }

  if PtInRect( ClientRect, Point( X, Y ) ) then
  begin
    // Click inside ClientRect
  end
  else if PtInRect( ControlRect, Point( X, Y ) ) then
  begin

  end
  else if Assigned( DropDown ) then
    DropDown.DroppedDown := False;
end;

procedure TPopupControl.MouseMove( Shift: TShiftState; X, Y: Integer );
begin
  inherited;

end;

procedure TPopupControl.MouseUp( Button: TMouseButton; Shift: TShiftState; X, Y: Integer );
begin
  if csDestroying in ComponentState then
    Exit;
  if psClosing in FPopupState then
    Exit;

  inherited;

  if FMouseDown and PtInRect( ClientRect, Point( X, Y ) ) then
  begin
    { Set State }
    Include( FPopupState, psClosing );

    if Assigned( DropDown ) then
    begin
      { Close & Destroy }
      DropDown.DroppedDown := False;
      Exit;
    end;
  end
  else if PtInRect( ControlRect, Point( X, Y ) ) then
  begin

  end
  else
    SetCaptureControl( Self );

  FMouseDown := False; { ! }
end;

procedure TPopupControl.Popup( const Location: TPoint; Anchor: TPopupAnchor = paLeft );
begin
  Popup( Location.X, Location.Y, Anchor );
end;

procedure TPopupControl.SetDropDown( const Value: IDropDown );
begin
  FDropDown := Value;
end;

procedure TPopupControl.SetShowShadow( const Value: Boolean );
begin
  if Value <> FShowShadow then
  begin
    FShowShadow := Value;

    { Call CreateParams }
    RecreateWnd;
  end;
end;

procedure TPopupControl.UpdatePopup;
begin
  inherited;

end;

procedure TPopupControl.Popup( const X, Y: Integer; Anchor: TPopupAnchor = paLeft );
var
  HCurrMonitor: HMONITOR;
  MonitorInf: MONITORINFO;
  PopupRect, MonRect: TRect;
  Ctrl: TControl;
  NX, NY: Integer;
  CursorPos: TPoint;
begin
  { Note: Adjust size
    before show! }
  BeforePopup;

  case Anchor of
    paLeft: PopupRect := Bounds( X, Y, Width, Height );
    paRight: PopupRect := Bounds( X - Width, Y, Width, Height );
    paRightTop: PopupRect := Bounds( X, Y, Width, Height );
  end;

  NX := PopupRect.Left;
  NY := Y;

  HCurrMonitor := MonitorFromRect( @PopupRect, MONITOR_DEFAULTTONEAREST );
  MonitorInf.cbSize := SizeOf( MonitorInf );
  GetMonitorInfo( HCurrMonitor, @MonitorInf );

  MonRect := MonitorInf.rcWork;

  Ctrl := TControl( Owner );

  if PopupRect.Left < MonRect.Left then
    NX := MonRect.Left;
  if PopupRect.Right > MonRect.Right then
    NX := MonRect.Right - Width;
  if PopupRect.Top < MonRect.Top then
    NY := MonRect.Top;
  if PopupRect.Bottom > MonRect.Bottom then
    NY := Y - Ctrl.Height - Height;

  Ctrl := TControl( Owner );

  // if ( NY < Y ) and ( NY + Height > Y + Ctrl.Height ) then
  // begin
  // NY := Y - Height;
  // NY := NY - Ctrl.Height;
  // end;

  SetWindowPos( Handle, HWND_TOP, NX, NY, Width, Height, SWP_SHOWWINDOW );

  Visible := True;

  AfterPopup;

  Repaint;

  GetCursorPos( CursorPos );

  if not PtInRect( BoundsRect, CursorPos ) then
  begin
    SetCaptureControl( Self );
  end;
end;

procedure TPopupControl.WMNCMouseLeave( var Message: TMessage );
begin
  inherited;
  SetCaptureControl( Self );
end;

procedure TPopupControl.WMNCPaint( var Message: TMessage );
var
  DC: HDC;
  Brush: HBRUSH;
  R: TRect;
begin
  DC := GetWindowDC( Handle );
  GetWindowRect( Handle, R );
  OffsetRect( R, -R.Left, -R.Top );
  Brush := CreateSolidBrush( ColorToRGB( clPopupBorder ) );
  FrameRect( DC, R, Brush );
  DeleteObject( Brush );
  ReleaseDC( Handle, DC );
end;

procedure TPopupControl.WndProc( var Message: TMessage );
begin
  inherited;
  case message.Msg of
    WM_ACTIVATEAPP:
      if TWMActivateApp( message ).Active = False then
        DropDown.DroppedDown := False;
  end;
end;

{ TNxEditPopupControl6 }

procedure TEditPopupControl.Close( Value: WideString );
begin
  Edit.Text := Value;
  inherited;
end;

constructor TEditPopupControl.Create( AOwner: TComponent );
begin
  inherited;
  FEdit := TCustomEdit( AOwner );
end;

{ TNxListPopupControl6 }

function TListPopupControl.GetPageItemCount: Integer;
begin
  Result := Min( DropDownCount, Count );
end;

procedure TListPopupControl.KeyDown( var Key: Word; Shift: TShiftState );
var
  Index: Integer;
begin
  inherited;
  case Key of
    VK_NEXT:
      begin
        if ItemIndex < VertScrollBar.Position + Pred( PageItemCount ) then
          index := VertScrollBar.Position + Pred( PageItemCount )
        else
          index := ItemIndex + PageItemCount;

        ItemIndex := Min( index, Pred( Count ) );
      end;
    VK_PRIOR:
      begin
        if ItemIndex > VertScrollBar.Position then
          index := VertScrollBar.Position
        else
          index := ItemIndex - PageItemCount;

        ItemIndex := Max( index, 0 );
      end;
  end;
end;

procedure TListPopupControl.Paint;
begin
  inherited Paint;
  Canvas.Font.Assign( Font );
end;

function TListPopupControl.PaintCell( const Index: Integer; CellRect: TRect; State: TOwnerDrawState ): Boolean;
var
  Ref: IStringsControl;
begin
  Result := True;
  if Supports( Edit, IStringsControl, Ref ) then
  begin
    Result := Ref.PaintCell( index, CellRect, State );
  end;
end;

procedure TListPopupControl.ScrollDown;
begin
  VertScrollBar.Prior;
end;

procedure TListPopupControl.ScrollToItem( const Index: Integer );
begin
  if index < VertScrollBar.Position then
  begin
    VertScrollBar.MoveBy( index - VertScrollBar.Position );
  end
  else if index > VertScrollBar.Position + Pred( PageItemCount ) then
  begin
    VertScrollBar.Position := Succ( index - DropDownCount );
  end;
end;

procedure TListPopupControl.ScrollUp;
begin
  VertScrollBar.Next;
end;

procedure TListPopupControl.UpdateVertScrollBar;
begin

end;

{ TNxStringsPopupControl6 }

procedure TStringsPopupControl.AfterPopup;
begin
  UpdateVertScrollBar;
end;

procedure TStringsPopupControl.BeforePopup;
  function CalcClientHeight: Integer;
  var
    i: Integer;
  begin
    Result := 0;
    for i := VertScrollBar.Position to Pred( Min( DropDownCount, Count ) ) do
      Inc( Result, MeasureItemHeight( i, StringsControl.ListWidth ) );
  end;

begin
  Width := StringsControl.ListWidth;

  if Width = 0 then
    Width := Edit.Width;

  if Count > 0 then
  begin
    ClientHeight := CalcClientHeight;

    UpdateVertScrollBar;

    VertScrollBar.Position := Succ( ItemIndex - DropDownCount );
    HotIndex := ItemIndex;
  end
  else
    ClientHeight := 2;

end;

constructor TStringsPopupControl.Create( AOwner: TComponent );
begin
  inherited;
  FHotIndex := -1;

  { Initialize }
  ScrollBars := [sbVertical];
  Width := 200;
  Height := 200;
end;

function TStringsPopupControl.GetCount: Integer;
begin
  Result := Items.Count;
end;

function TStringsPopupControl.GetDropDownCount: Integer;
begin
  Result := StringsControl.DropDownCount;
end;

function TStringsPopupControl.GetItemHeight: Integer;
begin
  Result := StringsControl.ItemHeight;
end;

function TStringsPopupControl.GetItemIndex: Integer;
begin
  Result := StringsControl.ItemIndex;
end;

function TStringsPopupControl.GetItems: TStrings;
begin
  Result := StringsControl.Items;
end;

function TStringsPopupControl.GetStringsControl: IStringsControl;
begin
  Supports( Owner, IStringsControl, Result );
end;

function TStringsPopupControl.MeasureItemHeight( const Index: Integer; const AWidth: Integer ): Integer;
begin
  Result := StringsControl.MeasureItemHeight( index, AWidth );
end;

procedure TStringsPopupControl.MouseMove( Shift: TShiftState; X, Y: Integer );
begin
  inherited;
  if PtInRect( ClientRect, Point( X, Y ) ) then
  begin
    HotIndex := VertScrollBar.Position + ( Y div ItemHeight );
  end;
end;

procedure TStringsPopupControl.MouseUp( Button: TMouseButton; Shift: TShiftState; X, Y: Integer );
begin
  if PtInRect( ClientRect, Point( X, Y ) ) then
  begin
    ItemIndex := VertScrollBar.Position + ( Y div ItemHeight );
  end;
  inherited;
end;

procedure TStringsPopupControl.Paint;
var
  i, Y: Integer;
  ItemRect: TRect;
  State: TOwnerDrawState;
begin
  inherited Paint;

  { Start }
  i := VertScrollBar.Position;
  Y := 0;

  if Count = 0 then
    EraseBkGnd( ClientRect )
  else
    while i < Count do
    begin
      ItemRect := Bounds( 0, Y, ClientWidth, ItemHeight );

      State := [];
      if i = HotIndex then
        State := State + [odHotLight];
      if i = ItemIndex then
        State := State + [odSelected];

      PaintCell( i, ItemRect, State );

      Inc( i );
      Inc( Y, ItemHeight );

      { Done }
      if Y > ClientHeight then
        Break;
    end;
end;

procedure TStringsPopupControl.RefreshItem( const Index: Integer );
begin
  if InRange( index, 0, Pred( Count ) ) then
    InvalidateRect( Bounds( 0, ( index - VertScrollBar.Position ) * ItemHeight, ClientWidth, ItemHeight ) );
end;

procedure TStringsPopupControl.ScrollContentBy( DeltaX, DeltaY: Integer );
begin
  ScrollRect( 0, -ItemHeight * DeltaY, ClientRect, ClientRect );
end;

procedure TStringsPopupControl.ScrollRect( DeltaX, DeltaY: Integer; Rect, ClipRect: TRect );
begin
{$IFDEF UNSAFE}
  ScrollWindowEx( Handle, DeltaX, DeltaY, @Rect, @ClipRect, 0, nil, SW_INVALIDATE );
{$ELSE}
  ScrollWindowEx( Handle, DeltaX, DeltaY, Rect, ClipRect, 0, nil, SW_INVALIDATE );
{$ENDIF}
end;

procedure TStringsPopupControl.SetHotIndex( const Value: Integer );
var
  T: Integer;
begin
  if Value <> FHotIndex then
  begin
    T := FHotIndex;
    FHotIndex := Value;

    RefreshItem( T );
    RefreshItem( FHotIndex );
  end;
end;

procedure TStringsPopupControl.SetItemIndex( const Value: Integer );
begin
  { Set ItemIndex & call Event }
  StringsControl.TryItemIndex( Value, True );
end;

procedure TStringsPopupControl.UpdatePopup;
begin
  inherited;
  HotIndex := ItemIndex;
  ScrollToItem( ItemIndex );
  Repaint;
end;

procedure TStringsPopupControl.UpdateVertScrollBar;
begin
  VertScrollBar.Max := Pred( Count );
  VertScrollBar.PageSize := PageItemCount;
end;

{ TNxCheckPopupControl6 }

function TNxCheckPopupControl6.GetCheckBoxAtPos( X, Y: Integer ): Integer;
begin
  Result := -1;
  if PtInRect( ClientRect, Point( X, Y ) ) then
  begin
    Result := VertScrollBar.Position + ( Y div ItemHeight );

    { Not inside checkbox? }
    if not PtInRect( GetCheckBoxRect( Result ), Point( X, Y ) ) then
      Result := -1;
  end;
end;

function TNxCheckPopupControl6.GetCheckBoxRect( const Index: Integer ): TRect;
var
  Container: TRect;
  CheckBoxLocation: TPoint;
begin
  Container := Bounds( 0, ( index - VertScrollBar.Position ) * ItemHeight, ClientWidth, ItemHeight );
  Container.Right := Container.Left + CheckBoxSize.cx + SpacingDouble;

  // CheckBoxLocation := PosInRect( CheckBoxSize.cx, CheckBoxSize.cy, Container, Classes.taCenter, taVerticalCenter );

  Result := Bounds( CheckBoxLocation.X, CheckBoxLocation.Y, CheckBoxSize.cx, CheckBoxSize.cy );
end;

function TNxCheckPopupControl6.GetCheckBoxSize: TSize;
begin
  Result := TSize.Create( 11, 11 );
end;

procedure TNxCheckPopupControl6.MouseDown( Button: TMouseButton; Shift: TShiftState; X, Y: Integer );
var
  Index: Integer;
  Reference: ICheckPopupOwner;
begin
  index := GetCheckBoxAtPos( X, Y );
  if index <> -1 then
  begin
    if Supports( FEdit, ICheckPopupOwner, Reference ) then
    begin
      Reference.CheckBoxClick( index );
      RefreshCheckBox( index );
    end;
  end;
  inherited;
end;

procedure TNxCheckPopupControl6.MouseUp( Button: TMouseButton; Shift: TShiftState; X, Y: Integer );
begin
  { Not click on checkbox? Then, close }
  if GetCheckBoxAtPos( X, Y ) = -1 then
    inherited;
end;

procedure TNxCheckPopupControl6.RefreshCheckBox( const Index: Integer );
begin
  { Refresh }
  InvalidateRect( GetCheckBoxRect( index ) );
end;

{ TNxDatePickerView }

procedure TDatePickerView.Changed;
var
  InvalidationRect: TRect;
begin
  case FSelectionMode of
    dmDays: InvalidationRect := DaysRect;
    dmMonths: InvalidationRect := MonthsRect;
  else InvalidationRect := ContentRect;
  end;

  Winapi.Windows.InvalidateRect( Control.Handle, InvalidationRect, False );

  InvalidationRect := HeaderTitleRect;
  Winapi.Windows.InvalidateRect( Control.Handle, InvalidationRect, False );
end;

constructor TDatePickerView.Create;
begin
  FDown := False;
  FHotDay := 0;
  FHotMonth := -1;
  FPressedMonth := -1;
  FSelectionDateRange := drAll;
  FSelectionMode := dmDays;
  FValue := '';
end;

procedure TDatePickerView.DoCustomDrawDate( Date: TDateTime; DayRect: TRect );
begin
  if Assigned( FOnCustomDrawDate ) then
    FOnCustomDrawDate( Self, Date, DayRect );
end;

procedure TDatePickerView.DoGetDateColor( Date: TDateTime; var DayColor: TColor );
begin
  if Assigned( FOnGetDateColor ) then
    FOnGetDateColor( DatePopupControl, Date, DayColor );
end;

procedure TDatePickerView.FillDay( Date: TDateTime; DayRect: TRect; DayColor: TColor );
begin
  DoGetDateColor( Date, DayColor );
  with Canvas do
  begin
    Brush.Color := DayColor;
    FillRect( DayRect );
  end;
end;

function TDatePickerView.GetActualStartDay: TStartDayOfWeek;
var
  D: array [0 .. 1] of Char;
begin
  case FStartDay of
    dwSystem:
      begin
        GetLocaleInfo( LOCALE_USER_DEFAULT, LOCALE_IFIRSTDAYOFWEEK, D, SizeOf( D ) );
        case StrToInt( D ) of
          0: Result := dwMonday;
        else Result := dwSunday;
        end;
      end
  else Result := FStartDay;
  end;
end;

function TDatePickerView.GetContentRect: TRect;
begin
  Result := ClientRect;
  Result.Top := GetHeaderRect.Bottom;
end;

function TDatePickerView.GetDatePopupControl: TDatePopupControl;
begin
  Result := TDatePopupControl( Control );
end;

function TDatePickerView.GetDayAtPos( const X, Y: Integer ): TDateTime;
var
  Pos: TPoint;
  DayRect: TRect;
  j, i: Integer;
begin
  { Start }
  Pos := DaysRect.TopLeft;

  Result := EncodeDate( Year, Month, 1 );

  { Not a first in week }
  if GetDayOfWeek( StartOfAMonth( Year, Month ) ) > 1 then
  begin
    { 1 - 7 }
    Result := IncDay( Result, -Pred( GetDayOfWeek( StartOfAMonth( Year, Month ) ) ) );
  end;

  for i := 1 to 6 do
  begin
    for j := 1 to 7 do
    begin
      DayRect := Bounds( Pos.X, Pos.Y, DayWidth, DayHeight );

      { Done? }
      if PtInRect( DayRect, Point( X, Y ) ) then
        Exit;

      Result := IncDay( Result );

      Inc( Pos.X, DayWidth );
    end;

    Inc( Pos.Y, DayHeight );

    Pos.X := DaysRect.Left;
  end;

  { None }
  Result := 0;
end;

function TDatePickerView.GetDayHeight: Integer;
begin
  Result := DotToPixel( FDayHeight );
end;

function TDatePickerView.GetDayRect( const Date: TDateTime ): TRect;
var
  Pos: TPoint;
  Day: TDateTime;
  j, i: Integer;
begin
  { Invalid Date }
  if ( Date = 0 ) or ( Year = 0 ) or ( Month = 0 ) then
    Exit;

  { Start }
  Pos := DaysRect.TopLeft;

  Day := EncodeDate( Year, Month, 1 );

  { Not a first in week }
  if GetDayOfWeek( StartOfAMonth( Year, Month ) ) > 1 then
  begin
    Day := IncDay( Day, -Pred( GetDayOfWeek( StartOfAMonth( Year, Month ) ) ) );
  end;

  for i := 1 to 6 do
  begin
    for j := 1 to 7 do
    begin
      Result := Bounds( Pos.X, Pos.Y, DayWidth, DayHeight );

      { Done? }
      if Day = Date then
        Exit;

      Day := IncDay( Day );

      Inc( Pos.X, DayWidth );
    end;

    Inc( Pos.Y, DayHeight );

    Pos.X := DaysRect.Left;
  end;

  { None }
  Result := Bounds( 0, 0, 0, 0 );
end;

function TDatePickerView.GetDaysRect: TRect;
var
  X: Integer;
begin
  X := 8;
  Result := Bounds( DotToPixel( X ), WeekDaysRect.Bottom, DayWidth * 7, DayHeight * 6 );
end;

function TDatePickerView.GetDayWidth: Integer;
begin
  Result := DotToPixel( FDayWidth );
end;

/// <summary>
/// Gets compete header rectangle including arrows.
/// </summary>
function TDatePickerView.GetHeaderRect: TRect;
begin
  Result := Rect( 1, 1, ClientRect.Right - 1, DayHeight * 2 );
end;

function TDatePickerView.GetHeaderTitleRect: TRect;
begin
  Result := Rect( GetLeftArrowRect.Right, HeaderRect.Top, GetRightArrowRect.Left, HeaderRect.Bottom );
end;

function TDatePickerView.GetLeftArrowRect: TRect;
begin
  with Result do
  begin
    Left := 0;
    Top := 0;
    Bottom := HeaderRect.Bottom;
    Right := Bottom;
  end;
end;

function TDatePickerView.GetMonthAtPos( const X, Y: Integer ): Integer;
var
  TopLeft: TPoint;
  c, R: Integer;
begin
  if PtInRect( MonthsRect, Point( X, Y ) ) then
  begin
    TopLeft := MonthsRect.TopLeft;
    c := ( ( X - TopLeft.X ) div MonthSize.cx ) + 1;
    R := ( Y - TopLeft.Y ) div MonthSize.cy;
    Result := c + R * 4;
  end
  else
    Result := -1;
end;

function TDatePickerView.GetMonthName( Month: Integer ): WideString;
begin
  Result := FormatSettings.LongMonthNames[Month];
end;

function TDatePickerView.GetMonthRect( const AMonth: Integer ): TRect;
var
  c, R: Integer;
begin
  R := Pred( AMonth ) div 4;
  c := Pred( AMonth ) mod 4;
  Result := Bounds( c * MonthSize.cx, R * MonthSize.cy, MonthSize.cx, MonthSize.cy );
  OffsetRect( Result, MonthsRect.Left, MonthsRect.Top );
end;

function TDatePickerView.GetMonthSize: TSize;
begin
  with Result do
  begin
    cx := ( RectWidth( ContentRect ) - DotToPixel( 4 ) ) div 4;
    cy := DayHeight * 7 div 3;
  end;
end;

function TDatePickerView.GetMonthsRect: TRect;
begin
  Result := Bounds( DotToPixel( 3 ), HeaderRect.Bottom, MonthSize.cx * 4, MonthSize.cy * 4 );
end;

function TDatePickerView.GetRightArrowRect: TRect;
begin
  with Result do
  begin
    Left := ClientRect.Right - HeaderRect.Bottom;
    Top := 0;
    Right := ClientRect.Right;
    Bottom := HeaderRect.Bottom;
  end;
end;

function TDatePickerView.GetWeekDaysRect: TRect;
var
  X: Integer;
begin
  X := 8;
  Result := Bounds( DotToPixel( X ), HeaderRect.Bottom, ClientRect.Right, DayHeight );
end;

function TDatePickerView.GetWeeksRect: TRect;
begin
  Result := Bounds( DotToPixel( 8 ), DaysRect.Top, DayWidth, DayHeight * 6 );
end;

function TDatePickerView.GetYearAtPos( const X, Y: Integer ): Integer;
var
  Offset: TPoint;
  c, R: Integer;
begin
  Result := 0;
  Offset := ContentRect.TopLeft;
  if PtInRect( Bounds( Offset.X, HeaderRect.Bottom, MonthSize.cx * 4, MonthSize.cy * 3 ), Point( X, Y ) ) then
  begin
    c := ( X - Offset.X ) div MonthSize.cx;
    R := ( Y - Offset.Y ) div MonthSize.cy;

    Result := Year + ( c + R * 4 ) - 1;
  end;
end;

function TDatePickerView.GetYearSize: TSize;
begin
  with Result do
  begin
    cx := ( RectWidth( ContentRect ) - 10 ) div 4;
    cy := DayHeight * 7 div 3;
  end;
end;

function TDatePickerView.IsShown( Date: TDateTime ): Boolean;
var
  Day: TDateTime;
  j, i: Integer;
begin
  Result := False;

  Day := EncodeDate( Year, Month, 1 );

  { Not a first in week }
  if GetDayOfWeek( StartOfAMonth( Year, Month ) ) > 1 then
  begin
    { Prev. month }
    Day := IncDay( Day, -GetDayOfWeek( StartOfAMonth( Year, Month ) ) );
  end;

  for i := 1 to 6 do
    for j := 1 to 7 do
    begin
      { Done? }
      if Day = Date then
      begin
        Result := True;

        Exit;
      end;

      Day := IncDay( Day );
    end;
end;

class function TDatePickerView.IsValidDateSelection( const Date: TDateTime; const SelectionDateRange: TDateRange ): Boolean;
begin
  case SelectionDateRange of
    drPastOnly: Result := Date <= Today;
    drFutureOnly: Result := Date >= Today;
  else Result := True;
  end;
end;

procedure TDatePickerView.MouseDown( Button: TMouseButton; Shift: TShiftState; X, Y: Integer );
begin
  if PtInRect( GetLeftArrowRect, Point( X, Y ) ) then
  begin
    DatePopupControl.PrevMonth;
  end;

  if PtInRect( GetRightArrowRect, Point( X, Y ) ) then
  begin
    DatePopupControl.NextMonth;
  end;

  if PtInRect( GetHeaderTitleRect, Point( X, Y ) ) then
  begin
    case SelectionMode of
      dmDays: SelectionMode := dmMonths;
      dmMonths: SelectionMode := dmYears;
    else SelectionMode := dmDays;
    end;
  end;

  if PtInRect( ContentRect, Point( X, Y ) ) then
  begin
    case SelectionMode of
      dmDays:;
      dmMonths: PressedMonth := GetMonthAtPos( X, Y );
    else;
    end;
  end;

  Down := True;
end;

procedure TDatePickerView.MouseLeave;
begin
  inherited;
  HotDay := 0;
  HotMonth := -1;
end;

procedure TDatePickerView.MouseMove( Shift: TShiftState; X, Y: Integer );
begin
  case SelectionMode of
    dmDays: HotDay := GetDayAtPos( X, Y );
    dmMonths:
      begin
        HotMonth := GetMonthAtPos( X, Y );
        if Down then
          PressedMonth := HotMonth;
      end;
    dmYears:;
  end;
end;

procedure TDatePickerView.MouseUp( Button: TMouseButton; Shift: TShiftState; X, Y: Integer );
begin
  case SelectionMode of
    dmDays:
      { Inside days click? }
      if PtInRect( DaysRect, Point( X, Y ) ) and Down then
      begin
        if IsValidDateSelection( HotDay, SelectionDateRange ) then
        begin
          { Event }
          DatePopupControl.DropDown.DoSelect;

          { Assign value & close? }
          FValue := DateToStr( HotDay );
        end;
      end;
    dmMonths:
      begin
        if PtInRect( ContentRect, Point( X, Y ) ) then
        begin
          Month := GetMonthAtPos( X, Y );
          SelectionMode := dmDays;
        end;
        PressedMonth := -1;
      end;
    dmYears:
      if PtInRect( ContentRect, Point( X, Y ) ) then
        Year := GetYearAtPos( X, Y );
  end;
  FDown := False;
end;

function TDatePickerView.GetDayInWeekText( DayInWeek: Integer ): WideString;
begin
  case GetActualStartDay of
    dwSunday: Result := LeftStr( FormatSettings.LongDayNames[DayInWeek], 2 );
    dwMonday:
      if DayInWeek = 7 then
        Result := LeftStr( FormatSettings.LongDayNames[1], 2 )
      else
        Result := LeftStr( FormatSettings.LongDayNames[DayInWeek + 1], 2 );
  end;

end;

function TDatePickerView.GetDayOfWeek( Day: TDateTime ): Integer;
begin
  case GetActualStartDay of
    dwSunday: Result := DayOfWeek( Day );
  else Result := DayOfTheWeek( Day );
  end;
end;

procedure TDatePickerView.Paint;
begin
  PaintHeader;
  case FSelectionMode of
    dmDays: PaintDaysView( DaysRect );
    dmMonths: PaintMonthsView( MonthsRect );
    dmYears: PaintYearsView( ContentRect );
  end;
end;

procedure TDatePickerView.PaintCell( const CellRect: TRect; const State: TButtonState; const Text: string );
var
  fillColor: TColor;
  textRect: TRect;
  cellText: string;
begin
  with Canvas do
  begin
    fillColor := Color;
    if btSelected in State then
    begin
      Font.Color := clWindowText;
      fillColor := clDatePickerToday;
      Brush.Color := fillColor;
      Pen.Color := clDatePickerTodayBorder;
      Rectangle( CellRect );
    end
    else
    begin
      Font.Color := clWindowText;
      if btPushed in State then
      begin
        Brush.Color := clBtnFace;
        FillRect( CellRect );
      end
      else
      begin
        Brush.Color := fillColor;
        FillRect( CellRect );
        if btHot in State then
        begin
          Brush.Color := clDatePickerDayHot;
          if btDisabled in State then
            Pen.Color := clSilver;
          FillRect( CellRect );
        end;
      end;
    end;
  end;
  textRect := CellRect;
  Dec( textRect.Right, SpacingDouble + 1 );
  cellText := Text;
  Canvas.RenderText( textRect, cellText, [tfRight, tfSingleLine, tfVerticalCenter] );
end;

procedure TDatePickerView.PaintDay( DayRect: TRect; Day: TDateTime );
var
  Yr, Mo, Dy: Word;
  TodayRect, TxtRect: TRect;
  fillColor: TColor;
  ValidDateSelection: Boolean;
  dayText: string;
begin
  with Canvas do
  begin
    ValidDateSelection := IsValidDateSelection( Day, SelectionDateRange );

    Font.Color := clWindowText;

    if MonthOf( Day ) <> Month then
      Font.Color := clGrayText;

    if not IsValidDateSelection( Day, SelectionDateRange ) then
      Font.Color := clSilver;

    fillColor := Color;
    if Down then
    begin
      if Day = HotDay then
      begin
        fillColor := IfThen( ValidDateSelection, clBtnFace, clBtnFace );
      end;

    end
    else
    begin
      if Day = Selected then
        fillColor := clSelectedBackground;
    end;

    FillDay( Day, DayRect, fillColor );

    if ( Day = HotDay ) and not Down then
    begin
      Brush.Color := clDatePickerDayHot;
      if not ValidDateSelection then
        Brush.Color := clSilver;
      FillRect( Rect( DayRect.Left, DayRect.Top, DayRect.Right, DayRect.Bottom ) );
    end;

    if Day = Today then
    begin
      TodayRect := DayRect;
      TodayRect.Inflate( -2, -2 );
      Brush.Color := clDatePickerToday;
      Pen.Color := clDatePickerToday;
      FillRect( TodayRect );
      Font.Color := clWindowText;
    end;

    DecodeDate( Day, Yr, Mo, Dy );

    { Right Margin }
    TxtRect := DayRect;
    Dec( TxtRect.Right, SpacingDouble + 1 );

    dayText := IntToStr( Dy );

    Canvas.textRect( TxtRect, dayText, [tfRight, tfSingleLine, tfVerticalCenter] );

    DoCustomDrawDate( Day, DayRect );
  end;
end;

procedure TDatePickerView.PaintDays( const X, Y: Integer );
var
  Pos: TPoint;
  DayRect: TRect;
  Day: TDateTime;
  j, i: Integer;
begin
  Pos := Point( X, Y );

  Day := EncodeDate( Year, Month, 1 );

  { Not first in week? }
  if GetDayOfWeek( StartOfAMonth( Year, Month ) ) > 1 then
  begin
    { 1 - 7 }
    Day := IncDay( Day, -Pred( GetDayOfWeek( StartOfAMonth( Year, Month ) ) ) );
  end;

  for i := 1 to 6 do
  begin
    for j := 1 to 7 do
    begin
      DayRect := Bounds( Pos.X, Pos.Y, DayWidth, DayHeight );

      PaintDay( DayRect, Day );

      Day := IncDay( Day );

      Inc( Pos.X, DayWidth );
    end;

    Inc( Pos.Y, DayHeight );

    Pos.X := X;
  end;
end;

procedure TDatePickerView.PaintDaysView( const AContentRect: TRect );
var
  Pos: TPoint;
begin
  { Days of week }
  Pos := WeekDaysRect.TopLeft;
  PaintWeekDays( Pos.X, Pos.Y );

  { Days }
  Pos := DaysRect.TopLeft;
  PaintDays( Pos.X, Pos.Y );
end;

procedure TDatePickerView.PaintHeader;
var
  R: TRect;
  Mid, FontSize: Integer;
begin
  R := HeaderRect;

  with Canvas do
  begin
    Font.Color := clWindowText;
    Font.Style := [];
    FontSize := Font.Size;
    Font.Size := 12;

    { Title }
    Canvas.RenderText( HeaderRect, GetMonthName( Month ) + ' ' + IntToStr( Year ), [tfCenter, tfSingleLine, tfVerticalCenter] );

    Brush.Color := $00444444;
    Pen.Color := Brush.Color;

    { Shape Rect }
    R := Bounds( 0, 0, DotToPixel( 8 div 2 ), DotToPixel( 8 ) );

    { Left arrow }
    R := CenteredRect( GetLeftArrowRect, R );
    with R do
    begin
      Mid := Top + RectHeight( R ) div 2;
      Polygon( [Point( Left, Mid ), Point( Right, Top ), Point( Right, Bottom )] );
    end;

    { Right arrow }
    R := CenteredRect( GetRightArrowRect, R );
    with R do
    begin
      Polygon( [Point( Left, Top ), Point( Right, Mid ), Point( Left, Bottom )] );
    end;

    Font.Size := FontSize;
  end;
end;

procedure TDatePickerView.PaintMonthsView( const AContentRect: TRect );
var
  i, X, Y: Integer;
  MonthRect: TRect;
  State: TButtonState;
  S: string;
begin
  X := AContentRect.Left;
  Y := AContentRect.Top;
  for i := 1 to 12 do
  begin
    MonthRect := Bounds( X, Y, MonthSize.cx, MonthSize.cy );

    S := GetMonthName( i );
    S := LeftStr( S, 3 );

    State := [];
    if i = Month then
      Include( State, btSelected );
    if i = HotMonth then
      Include( State, btHot );
    if i = PressedMonth then
      Include( State, btPushed );

    PaintCell( MonthRect, State, S );

    Inc( X, MonthSize.cx );
    if i mod 4 = 0 then
    begin
      X := AContentRect.Left;
      Inc( Y, MonthSize.cy );
    end;
  end;
end;

procedure TDatePickerView.PaintWeekDays( const X, Y: Integer );
var
  i: Integer;
  Pos: TPoint;
  DayRect, TxtRect: TRect;
  dayText: string;
begin
  { Start }
  Pos := Point( X, Y );

  { Dim font }
  Canvas.Font.Color := clGrayText;

  for i := 1 to 7 do
  begin
    DayRect := Bounds( Pos.X, Pos.Y, DayWidth, DayHeight );

    TxtRect := DayRect;
    Dec( TxtRect.Right, SpacingDouble + 1 );

    dayText := WideLowerCase( GetDayInWeekText( i ) ) + '.';

    Canvas.Brush.Style := bsClear;
    Canvas.textRect( TxtRect, dayText, [tfRight, tfSingleLine, tfVerticalCenter] );
    Canvas.Brush.Style := bsSolid;

    Inc( Pos.X, DayWidth );
  end;
end;

procedure TDatePickerView.PaintYearsView( const AContentRect: TRect );
var
  i, X, Y, Yr: Integer;
  YearRect: TRect;
  State: TButtonState;
  S: string;
begin
  Yr := Year - 1;
  X := AContentRect.Left;
  Y := AContentRect.Top;
  for i := 1 to 12 do
  begin
    YearRect := Bounds( X, Y, YearSize.cx, YearSize.cy );

    S := IntToStr( Yr );

    State := [];
    if Yr = Year then
      Include( State, btSelected );

    PaintCell( YearRect, State, S );

    Inc( Yr );

    Inc( X, YearSize.cx );
    if i mod 4 = 0 then
    begin
      X := AContentRect.Left;
      Inc( Y, YearSize.cy );
    end;

  end;
end;

procedure TDatePickerView.RefreshDay( const Date: TDateTime );
var
  R: TRect;
begin
  if IsShown( Date ) then
  begin
    R := GetDayRect( Date );
    Winapi.Windows.InvalidateRect( Control.Handle, R, False );
  end;
end;

procedure TDatePickerView.RefreshMonth( const AMonth: Integer );
begin
  if InRange( AMonth, 1, 12 ) then
    InvalidateRect( GetMonthRect( AMonth ) );
end;

procedure TDatePickerView.SetColor( const Value: TColor );
begin
  FColor := Value;
end;

procedure TDatePickerView.SetDayHeight( const Value: Integer );
begin
  FDayHeight := Value;
end;

procedure TDatePickerView.SetDayWidth( const Value: Integer );
begin
  FDayWidth := Value;
end;

procedure TDatePickerView.SetDown( const Value: Boolean );
begin
  if Value <> FDown then
  begin
    FDown := Value;
    RefreshDay( Selected );
    RefreshDay( FHotDay );
  end;
end;

procedure TDatePickerView.SetHotDay( const Value: TDateTime );
var
  T: TDateTime;
begin
  if Value <> FHotDay then
  begin
    T := FHotDay;
    FHotDay := Value;
    RefreshDay( T );
    RefreshDay( FHotDay );
  end;
end;

procedure TDatePickerView.SetHotMonth( const Value: Integer );
begin
  if Value <> FHotMonth then
  begin
    if InRange( FHotMonth, -1, 12 ) then
      InvalidateRect( GetMonthRect( FHotMonth ) );
    if InRange( Value, -1, 12 ) then
    begin
      FHotMonth := Value;
      if FHotMonth <> -1 then
        InvalidateRect( GetMonthRect( FHotMonth ) );
    end;
  end;
end;

procedure TDatePickerView.SetMonth( const Value: Word );
begin
  if Value <> FMonth then
  begin
    FMonth := Value;
    if FMonth < 1 then
    begin
      FMonth := 12;
      Dec( FYear );
    end;
    if FMonth > 12 then
    begin
      FMonth := 1;
      Inc( FYear );
    end;

    { Refresh }
    Changed;
  end;
end;

procedure TDatePickerView.SetPressedMonth( const Value: Integer );
begin
  if Value <> FPressedMonth then
  begin
    RefreshMonth( FPressedMonth );
    FPressedMonth := Value;
    RefreshMonth( FPressedMonth );
  end;
end;

procedure TDatePickerView.SetSelected( const Value: TDateTime );
var
  R1, R2: TRect;
begin
  if Value <> FSelected then
  begin
    R1 := GetDayRect( FSelected );

    FSelected := Value;
    FValue := DateToStr( FSelected );
    DatePopupControl.Edit.Text := FValue;

    if ( MonthOf( FSelected ) <> Month ) or ( YearOf( FSelected ) <> Year ) then
    begin
      Month := MonthOf( FSelected );
      Year := YearOf( FSelected );
    end;

    R2 := GetDayRect( FSelected );

    InvalidateRect( R1 );
    InvalidateRect( R2 );
  end;
end;

procedure TDatePickerView.SetSelectionMode( const Value: TDateSelectionMode );
begin
  if Value <> FSelectionMode then
  begin
    FSelectionMode := Value;
    InvalidateRect( ContentRect );
  end;
end;

procedure TDatePickerView.SetStartDay( const Value: TStartDayOfWeek );
begin
  FStartDay := Value;
end;

procedure TDatePickerView.SetYear( const Value: Word );
begin
  if Value <> FYear then
  begin
    FYear := Value;
    Changed;
  end;
end;

function TDatePickerView.TrySelect( const Date: TDateTime ): Boolean;
begin
  Result := False;
  if IsValidDateSelection( Date, SelectionDateRange ) then
  begin
    Selected := Date;
    Result := True;
  end;
end;

{ TNxDatePopupControl6 }

constructor TDatePopupControl.Create( AOwner: TComponent );
var
  Year, Month, Day: Word;
begin
  inherited;

  { Init. Buttons }
  FTodayButton := TdcGraphicButton.Create( Self );
  FTodayButton.Caption := StrToday;
  FTodayButton.Font.Assign( Font );
  FTodayButton.Parent := Self;
  FTodayButton.OnClick := DoButtonClick;
  FTodayButton.SetBounds( DotToPixel( 8 ), DotToPixel( 228 ), DotToPixel( 60 ), DotToPixel( 20 ) );

  FNoneButton := TdcGraphicButton.Create( Self );
  FNoneButton.Caption := StrReset;
  FNoneButton.Font.Assign( Font );
  FNoneButton.Parent := Self;
  FNoneButton.OnClick := DoButtonClick;
  FNoneButton.SetBounds( DotToPixel( 80 ), DotToPixel( 228 ), DotToPixel( 60 ), DotToPixel( 20 ) );

  fView := TDatePickerView.Create;
  fView.Control := Self;
  fView.SelectionDateRange := FSelectionDateRange;
  fView.StartDay := dwSystem;
  fView.Color := Color;

  fView.FSelected := StrToDateDef( Edit.Text, Today );
  DecodeDate( fView.Selected, Year, Month, Day );
  fView.Year := Year;
  fView.Month := Month;

  fView.Canvas := Canvas;
  fView.OnGetDateColor := DoGetDateColor;
  fView.OnCustomDrawDate := DoCustomDrawDate;

  { Init. Properties }
  Height := DotToPixel( GetBestSize.cy );
  Width := DotToPixel( GetBestSize.cx );
end;

destructor TDatePopupControl.Destroy;
begin
  FreeAndNil( FNoneButton );
  FreeAndNil( FTodayButton );
  FreeAndNil( fView );
  inherited;
end;

procedure TDatePopupControl.DoButtonClick( Sender: TObject );
begin
  if Sender = FNoneButton then
    Close( EmptyStr );
  if Sender = FTodayButton then
    Close( DateToStr( Today ) );
end;

procedure TDatePopupControl.DoCustomDrawDate( Sender: TObject; Date: TDateTime; DayRect: TRect );
begin
  PickerOwner.DoCustomDrawDate( Self, Date, DayRect );
end;

procedure TDatePopupControl.DoGetDateColor( Sender: TObject; Date: TDateTime; var DayColor: TColor );
begin
  PickerOwner.DoGetDateColor( Self, Date, DayColor );
end;

function TDatePopupControl.GetBestSize: TSize;
begin
  Result := TSize.Create( 208, 260 );
end;

function TDatePopupControl.GetPickerOwner: IDatePickerOwner;
begin
  Supports( Edit, IDatePickerOwner, Result );
end;

procedure TDatePopupControl.KeyDown( var Key: Word; Shift: TShiftState );
begin
  inherited;
  case Key of
    VK_LEFT:
      if ssShift in Shift then
        NextMonth
      else
        fView.TrySelect( IncDay( fView.Selected, -1 ) );
    VK_RIGHT:
      if ssShift in Shift then
        PrevMonth
      else
        fView.TrySelect( IncDay( fView.Selected, 1 ) );
    VK_UP: fView.TrySelect( IncWeek( fView.Selected, -1 ) );
    VK_DOWN: fView.TrySelect( IncWeek( fView.Selected, 1 ) );
  end;
end;

procedure TDatePopupControl.MouseDown( Button: TMouseButton; Shift: TShiftState; X, Y: Integer );
begin
  if PtInRect( ClientRect, Point( X, Y ) ) then
  begin
    fView.MouseDown( Button, Shift, X, Y );
  end;
  inherited;
end;

procedure TDatePopupControl.MouseMove( Shift: TShiftState; X, Y: Integer );
begin
  inherited;
  fView.MouseMove( Shift, X, Y );
end;

procedure TDatePopupControl.MouseUp( Button: TMouseButton; Shift: TShiftState; X, Y: Integer );
begin
  if PtInRect( ClientRect, Point( X, Y ) ) then
  begin
    if FMouseDown then
      fView.MouseUp( Button, Shift, X, Y );

    if fView.Value <> '' then
      Close( fView.Value );
  end
  else
    inherited;
end;

procedure TDatePopupControl.NextMonth;
begin
  fView.Month := fView.Month + 1;
end;

procedure TDatePopupControl.Paint;
begin
  inherited;
  fView.DayHeight := 24;
  fView.DayWidth := 27;

  try
    { Prepare }
    fView.ClientRect := ClientRect;

    with Canvas do
    begin
      Font.Assign( Self.Font );
      Brush.Color := clWindow;

      try
        { Prevent flickering }
        ExcludeClipRect( Canvas, fView.DaysRect );

        { Fill all excluding days }
        FillRect( ClientRect );
      finally
        SetClipRect( Canvas, ClientRect );
      end;
    end;

    { Paint }
    fView.Paint;
  except

  end;
end;

procedure TDatePopupControl.PrevMonth;
begin
  fView.Month := fView.Month - 1;
end;

procedure TDatePopupControl.SetSelectionDateRange( const Value: TDateRange );
begin
  FSelectionDateRange := Value;
  fView.SelectionDateRange := FSelectionDateRange;
end;

procedure TDatePopupControl.SetStartDayOfWeek( const Value: TStartDayOfWeek );
begin
  FStartDayOfWeek := Value;
  fView.StartDay := FStartDayOfWeek;
end;

end.
